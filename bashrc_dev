# git
gi(){
	if [[ $# -ne 1 ]]; then
		echo "[ERROR] Use like this: gi <URL>"
		return 1
	fi

	git init
	touch README.md
	touch .gitignore
	git add README.md .gitignore
	git commit -m "Initial commit"

	if [[ "$1" == "." ]]; then
		git remote add origin "git@gitlab.com:vali19th/$(basename $(pwd)).git"
		git push -u origin --all
		git push -u origin master --tags
	elif [[ -n "$1" ]]; then
		git remote add origin "git@gitlab.com:vali19th/$1.git"
		git push -u origin --all
		git push -u origin master --tags
	fi
}

gpa(){
	git diff "$1" > "$1".patch
}

gpap(){
	patch "$1" "$1".patch
}

gls(){
	branch=$(git branch | g '\*' | g -o '\w+$')
	git ls-tree $branch --name-status
}

glsr(){
	branch=$(git branch | g '\*' | g -o '\w+$')
	git ls-tree -r $branch --name-status
}

gacpa(){
	git pull && git add -u && git commit -m "$*" && git push --all
}
gacpf(){
	git pull && git add -f $1 && git commit -m "${@: -1}"  && git push --all
}
gaucp(){
	git pull && git add -u && git commit -m "${@: -1}"  && git push --all
}
gacp(){
	git pull && git add $1 && git commit -m "${@: -1}"  && git push --all
}
gac(){
	git add $1 && git commit -m "${@: -1}"
}
gacp2(){
	git add $1 && git commit -m "${@: -1}"  && git push --all
}
gcp(){
	git pull && git commit -m "$1" && git push --all
}
gcfix(){
	git pull && git commit --amend -m "$1" && git push --all
}
gcfixf(){
	git pull && git commit --amend -m "$1" && git push --all --force
}

gdh(){
	if (($#)); then
		git show HEAD~$@
	else
		git show HEAD
	fi
}

gdx(){
	git diff $1^!
}

gC(){
	git clone $1
    cd $(basename $1 .git)
}

gCgh(){
	git clone git@github.com:vali19th/$1.git
    cd $1
}

gCgl(){
	git clone git@gitlab.com:vali19th/$1.git
    cd $1
}

gCk(){
    git clone git@gitlab.com:katalytic/$1.git
    cd $1
}

gtv(){
	if (( $# == 1 )); then
        new_tag="v$1-$(date +%Y-%m-%d-%H%M%S)"
    else
        new_tag="v$(($(git tag -l | \grep -P "v\d+[-_]\d{4}-\d{2}-\d{2}" | sed 's/^v([0-9]+)[-_].*$/\1/' | sort -n | tail -n 1) + 1))-$(date +%Y-%m-%d-%H%M%S)"
    fi
    git tag "$new_tag"
    git push --prune --tags
}

gm() {
    [ "$#" -ne 2 ] && { echo "Error: Usage: gm <source_branch> <target_branch>"; return 1; }
    local src="$1"
    local dest="$2"

    # Check if the branches exist
    git rev-parse --verify "$src" &>/dev/null || { echo "Error: Source branch '$src' does not exist"; return 1; }
    git rev-parse --verify "$dest" &>/dev/null || { echo "Error: Destination branch '$dest' does not exist"; return 1; }

    git checkout "$dest" 2>/dev/null || { echo "Error: Failed to checkout '$dest'"; return 1; }
    git merge "$src" || { echo "Error: Merge failed. Resolve conflicts and try again."; return 1; }
    git branch -d "$src" || { echo "Error: Failed to delete local branch '$src'"; return 1; }
    git ls-remote --exit-code origin "$src" &>/dev/null && git push origin --delete "$src" || { echo "Warning: Failed to delete remote branch '$src'"; return 1; }

    echo "Successfully merged '$src' into '$dest' and deleted '$src'"
}

alias ga='git add'
alias gau='git add -u'
alias gap='git add -p'
alias gb='git branch'
alias gba='git branch -a'
alias gc='git commit -m'
alias gcf="grep -PnHs '<+ HEAD'"
alias gco='git checkout'
alias gd='git diff'
alias gdc='git diff --cached'
alias gds='git diff --stat'
alias gdsc='git diff --stat --cached'
alias gsh='git show'
alias gf='git fetch'
alias gl='git log --graph --pretty=format:"%Cred%h%Creset ~ %C(yellow)%ar%Creset ~ %C(green)%an%Creset %C(auto)%d%Creset | %Creset%s%Creset" --all'
alias gll='git log --graph --decorate=full --color=always --stat --all'
alias gld='git log --graph --decorate=full --color=always --stat --all -p'
alias gP='git pull --prune && git pull --prune --tags && git push --all --prune && git push --prune --tags'
alias gp='git pull --prune && git pull --prune --tags'
alias gR='git reset'
alias grs='git restore --staged'
alias gr='git rm --cached'
alias grm='git rm'
alias gS='git stash'
alias gs='git status'
alias gst='git status --untracked-files=no'
alias gsu='git push --all --set-upstream origin'
alias gt='git tag'
alias gus='git stash apply --index' # unstash

alias Gsb='gcp "style: black"'
alias Gsp='gcp "misc: spelling"'
alias GSP='original_dir=$(pwd) && cd ~/system/dotfiles && ga nvim/spell && gcp "misc: spelling" && cd $original_dir && exit'

# other
alias docker="sudo docker"
alias fixcuda='sudo rmmod nvidia_uvm && sudo modprobe nvidia_uvm'
alias wnv='watch -n 1 -dc nvidia-smi'
alias cpy="cloc . --match-f '\.py$'"
alias cipy="cloc . --match-f '\.ipynb$'"

scripts() {
	cd "$_DATA/system/scripts"
	ls --group-directories-first | sort
	if (($#)); then
		python generate_prototype.py $1;
		nvim $(ls -tp | grep .py | grep -v /$ | head -1)
	fi
}

main(){
	cd "$_DATA/system/scripts"
	nvim main.py
}

psqlu(){
	USERNAME=$(echo $1 | awk -F '[:@/]' '{print $4}')
	PASSWORD=$(echo $1 | awk -F '[:@/]' '{print $5}')
	HOST=$(echo $1 | awk -F '[:@/]' '{print $6}')
	PORT=$(echo $1 | awk -F '[:@/]' '{print $7}')
	DBNAME=$(echo $1 | awk -F '[:@/]' '{print $8}')

	# Starting psql
	PGPASSWORD=$PASSWORD psql -h $HOST -p $PORT -U $USERNAME -d $DBNAME "${@:2}"

	USERNAME=""
	PASSWORD=""
	HOST=""
	PORT=""
	DBNAME=""
}

t_htmx(){
	if [[ $# -ne 1 ]]; then
		echo "[ERROR] Use like this: t_htmx <name>"
		return 1
	fi

	rm -rf "$1" > /dev/null 2>&1
	gC git@gitlab.com:vali19th/template_fastapi_htmx.git "$1"
	cd "$1"

	rm -rf .git > /dev/null 2>&1
	vc

	git init
	git add .
	git commit -m "Initial commit"
	pip install --upgrade -r requirements.txt

	vi .
}

alias sqlite='sqlite3'
alias sql='sqlite3'

alias cdgo="cd $_DATA/study/go"
alias goo="command go"
alias GO='goo build -o build/ && ./build/$(basename $(pwd))'
alias GOT='goo test ./...'


mib_to_gb(){
	if (( $# != 1 )); then
		echo "Usage: mib_to_gb <MiB>"
		return 1
	fi

	mib=$(echo $1 | grep -Pio "\d+")  # remove the units if present
	echo "scale=6; $mib * 1.073741824 / 1024" | bc | awk '{printf "%.1f", $0}'
}

gpu() {
	data=$(nvidia-smi)
	echo -e "$data" | grep --color=no -Pio "CUDA Version: \d+\.\d+"
	echo -e "$data" | grep --color=no -Pio "Driver Version: \d+\.\d+(\.\d+)?"
	echo -e "GPUs:"
	echo "| GPU | Util % | Memory		|"
	echo "|-----|--------|---------------|"

	i=0
	memory=$(nvidia-smi --query-gpu=memory.used,memory.total --format=csv,noheader,nounits)
	utilization="$(nvidia-smi --query-gpu=utilization.gpu --format=csv,noheader,nounits)"
	echo -e "$memory" | while IFS= read -r mem; do
		mem_used=$(mib_to_gb $(echo $mem | awk -F, '{print $1}'))
		mem_total=$(mib_to_gb $(echo $mem | awk -F, '{print $2}'))
		printf "| %+3s | %+6s | %+5s / %+5s |\n" "$i" "${utilization[$i]}" "${mem_used}G" "${mem_total}G"
		((i++))
	done

	echo -e "\nProcesses:"
	gpu_proc "$data"
}
gpu_proc() {
	processes=$(nvidia-smi | grep -A 100 "Processes:" | grep -Pi "MiB")
	if [[ -z "$processes" ]]; then
		echo "No running processes found"
		return 1
	fi

	echo "| # |   PID   | GPU | Memory | Process								  |"
	echo "|---|---------|-----|--------|------------------------------------------|"

	i=0
	echo "$processes" | while IFS= read -r line; do
		gpu_id=$(echo "$line" | grep -Pio "^\s*\|\s+(\d+)" | grep -Pio "\d+$")
		pid=$(echo "$line" | grep -Pio "^\s*\|\s+\d+\s+\S+\s+\S+\s+(\d+)" | grep -Pio "\d+$")
		process_name=$(echo "$line" | grep -Pio "\.\.\.\S+\/\S+")
		memory_usage=$(echo "$line" | grep -Pio "\d+MiB" | grep -Pio "\d+")
		memory_usage="$(mib_to_gb $memory_usage)G"
		printf "| %s | %+7s | %+3s | %+6s | %-40s |\n" "$i" "$pid" "$gpu_id" "$memory_usage" "$process_name"
		((i++))
	done

	return 0
}

killgpu() {
	table=$(gpu_proc)
	echo -e "$table"
	if [[ "$table" == "No running processes found" ]]; then
		return
	fi

	procs=$(echo -e "$table" | tail -n +3)
	while true; do
		echo ""
		read -p "Enter the index of the process to kill (press 'Ctrl+C' to cancel): " num
		if [[ "$num" =~ ^[0-9]+$ ]] && [[ "$num" -lt "${#procs[@]}" ]]; then
			pid=$(echo -e "${procs[$num]}" | awk '{print $4}')
			echo "Killing process with PID $pid"
			sudo kill -9 $pid
			break
		else
			echo "Invalid process index ($num). Use the # column, not the PID"
		fi
	done
}

alias update_ollama="curl -fsSL https://ollama.com/install.sh | sh"
