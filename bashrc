# have two terminal shortcuts instead of custom code in bashrc to start it in a certain profile (light/dark)
# https://stackoverflow.com/questions/7442417/how-to-sort-an-array-in-bash
# - python
# - bash settings
#	- better defaults
#	 - history
#	 - PS1
#	 - PATH
#	 - funcs like is_truthy(), ask(), map(), fps(), resolution(), archive(), extract()
#	- utils (colors and stuff)
#	- grep, screen, git, apt, etc
# - sensitive ones. 1 file each line
#	 - shortcuts to go to a dir or open files
#	 - ssh/scp/sshfs/jupy
#		 - one per project
# after the splitup -> profile -> optimise


vmrss () {
  # if the number keeps going up, you've got a memory leak
	p=$1
	echo "pid choosen: $p"

	while true; do
		sync
		cat /proc/$p/status | grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn,.idea,.tox} VmRSS | grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn,.idea,.tox} -o '[0-9]\+' | awk '{ print $1/1024 " MB"}'
		sleep 1
	done
}


# ----------------------------------------------------------------------------------

set -a  # enable automatic export after this line
export EDITOR='nvim'
export VISUAL='nvim'

# expand "**" to match zero or more directories and subdirectories
shopt -s globstar

# history
HISTSIZE=100000
HISTFILESIZE=100000
HISTCONTROL=erasedups
HISTTIMEFORMAT="%y-%m-%d %T "
HISTIGNORE="h:history:clear:exit"
PROMPT_COMMAND="history -a; history -n; history -c; history -r; $PROMPT_COMMAND"
shopt -s histappend  # sync across multiple terminals


ask(){
	echo -e "\n${BLUE}${START}${RESET}"
	echo -e "${INFO} $(echo $@)\n" # print the command

	answer=''
	while ! [[ $answer =~ ^(y|n|yes|no)$ ]]; do # ask until you give one of those answers
		echo -n -e "\e[1A\e[K${INFO} Continue? (y/n) " # Reuse the same line for the prompt
		read answer
		answer=${answer,,} # make the answer lowercase
	done

	if [[ "$answer" =~ ^(y|yes)$ ]]; then
		echo -e "${INFO} Executing ..."
		"$@"
	else
		echo -e "${INFO} Skipping ..."
	fi

	echo -e "${BLUE}${END}${RESET}\n"
}

when(){
   stat --printf="	File: %n\n Created: %w\nModified: %y\n\n" "$@" | awk '{gsub(/\.[0-9]+ \+[0-9]+/, "", $0); print}'
}

# better defaults for common commands
alias df="df -h"
alias du="du -hs"
alias tree="tree --dirsfirst -I 'venv|*.pyc|__pycache__|*egg-info|.pytest_cache|.idea|.git'"
alias tr="tree"
alias trl="tr | less"
alias tr1="tr -L 1"
alias tr2="tr -L 2"
alias tr3="tr -L 3"
alias tr4="tr -L 4"
alias tr5="tr -L 5"
alias tr6="tr -L 6"
alias tr7="tr -L 7"
alias tr1l="tr1 | less"
alias tr2l="tr2 | less"
alias tr3l="tr3 | less"
alias tr4l="tr4 | less"
alias tr5l="tr5 | less"
alias tr6l="tr6 | less"
alias tr7l="tr7 | less"
alias tf="tail -f"
alias watch='watch -n1 -d'
alias ls='ls --group-directories-first -lLhv --color'
alias lsa='ls -a'
alias lsd='ls -d -- */'  # directories
alias lshi='ls -d .?* | g -v "\.\."'  # hidden folders and files
alias mkdir="mkdir -pv"
alias mv="mv -iv"
alias open="xdg-open"
alias o="xdg-open"
alias rename="rename -v -n"
alias renamey="command rename -v"
alias rm="rm -iv" # prompt before delete
alias rma="command rm -v"
alias cp='rsync -rahS --inplace --no-whole-file --info=progress2 --no-i-r --no-compress --update --exclude=".venv" --exclude=".git"'
alias cp2="command cp -riv" # the actual `cp` you're used to
alias sed="sed -E"
alias diff="diff --color=always -u" # make the output like `git diff`
alias less="less -R -FX" # preserve colors and if output fits on screen -> skip pagining
alias vi="nvim"
alias vim="nvim"
alias cat="bat --theme ansi"
alias catn="cat --color never"
alias clip="xclip -selection clipboard"
alias f="find . -regextype posix-egrep -regex"

pkill() {
    if [ -z "$1" ]; then
        echo "Usage: kill_process_by_keyword <keyword>"
        return 1
    fi

    local keyword="$1"
    local pid_list=()
    local command_list=()
    local current_pid=$$

    # Find processes matching the keyword, excluding this script
    while IFS= read -r line; do
        pid=$(echo "$line" | awk '{print $2}')
        command=$(echo "$line" | awk '{$1=$2=$3=$4=$5=$6=$7=$8=$9=$10=""; print $0}' | sed 's/^ *//')
        if [ "$pid" != "$current_pid" ]; then
            pid_list+=("$pid")
            command_list+=("$command")
        fi
    done < <(ps aux | g "$keyword" | g -v 'rg|grep')

    # Check if no processes matched
    if [ ${#pid_list[@]} -eq 0 ]; then
        echo "No processes found matching '$keyword'."
        return 1
    fi

    # Display processes with an index
    echo "Processes matching '$keyword':"
    for i in "${!pid_list[@]}"; do
        echo "$((i + 1))) ${command_list[$i]}"
    done

    echo -n "Which process to kill? "
    read -r selection

    if [ -z "$selection" ]; then
        echo "No process selected. Canceling."
        return 0
    fi

    # Validate input
    if ! [[ "$selection" =~ ^[0-9]+$ ]] || [ "$selection" -le 0 ] || [ "$selection" -gt ${#pid_list[@]} ]; then
        echo "Invalid selection. Canceling."
        return 1
    fi

    selected_pid="${pid_list[$((selection - 1))]}"
    echo "Killing process $selected_pid..."
    kill "$selected_pid" && echo "Process $selected_pid killed." || echo "Failed to kill process $selected_pid."
}

# watch
alias watch='watch -n1 -dc'
alias wtr='watch tree'
alias wls='watch "ls -lLth"'
alias wlg='watch "ls -lLth --group-directories-first | sort -f -k 9 | grep -Pi"'

# navigation
..(){
	cd "../$1"
}
..2(){
	cd "../../$1"
}
..3(){
	cd "../../../$1"
}
..4(){
	cd "../../../../$1"
}
..5(){
	cd "../../../../../$1"
}

duplicate(){
	if [[ $# -lt 2 ]]; then
		echo "[ERROR] Use: duplicate <src> <dest_1> [<dest_2> ...]"
		return
	fi

	for dest in "${@:2}"; do
		cp -r "$1" "$dest"
	done
}

# screen
alias sls="screen -ls"
alias ss="screen -S"
alias sjupy='screen -dmS jupy && screen -S jupy -X stuff "jupy\n"'

sr(){
	if (($#)); then
		screen -r $1
	else
		screen -ls
	fi
}

makedirs(){
	# USE: makedirs <source> <destination>
	for d in $(ls -x -1 --color=never $1); do
		mkdir -p "$2/$d"
	done
}

makedirsr(){
	# USE: makedirs <source> <destination>
	# recursive
	for d in $(find $1 -type d); do
		mkdir -p "$2/$d"
	done
}

# custom commands
fps(){
	ffmpeg -i "$1" 2>&1 | rg --color=never -Po '\d+ fps'
}

lsh(){
	ls $@ | head
}

lst(){
	ls $@ | tail
}

lsl(){
	ls $@ | less
}

ht(){
	if [[ -z "$1" ]]; then
		echo "[ERROR] Use: ht <src> [<n>]"
		return
	elif [[ -z "$2" ]]; then
		head $1
		echo "..."
		tail $1
	else
		head $1 -n $2
		echo "..."
		tail $1 -n $2
	fi
}

man() {
	env \
	LESS_TERMCAP_md=$'\e[1;36m' \
	LESS_TERMCAP_me=$'\e[0m' \
	LESS_TERMCAP_se=$'\e[0m' \
	LESS_TERMCAP_so=$'\e[1;40;92m' \
	LESS_TERMCAP_ue=$'\e[0m' \
	LESS_TERMCAP_us=$'\e[1;32m' \
		man "$@"
}

notify(){
	title="[$(date +%H:%M)] $@"
	message="$(history | tail -n1 | sed -e 's/^\s*[0-9]+\s+([-0-9: ]+\s*)?//;s/[;&|]\s*alert$//')"
	notify-send --urgency=critical -i "$([ $? = 0 ] && echo terminal || echo error)" "$title" "$message"
}

alias table="column -s, -t -L"
alias std="source /mnt/data/system/dotfiles/bashrc_std"
alias cdir="pwd | grep -Pio '[^/]+$' --color=no"
alias frames="ffprobe -v error -select_streams v:0 -count_packets -show_entries stream=nb_read_packets -of csv=p=0"
alias remount="sudo mount -a"
alias fixyt="yt-dlp -U; yt-dlp --rm-cache-dir"
alias fixhdd="sudo mount -a"
alias myip="hostname -I | grep -io '^[0-9.]+' | xclip -selection clipboard && xclip -o -selection clipboard"
alias ips="nmap -sn 192.168.1.0/24"
alias nospace='rm -rf $HOME/.xsession-errors*'
alias resolution='ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0'
alias s="rg -SnH --no-ignore-vcs"
alias sf="rg -SnH --no-ignore-vcs --files-with-matches"
alias S="rg -SnH --no-ignore-vcs --max-depth 1"
alias Sf="rg -SnH --no-ignore-vcs --max-depth 1 --files-with-matches"
alias mk="mkdir -pv"
alias count="find . -type f | cut -d/ -f2 | sort | uniq -c"
alias so="source $bash"
alias title="python '$_DATA/system/scripts/title_case.py'"
alias todos="s todo . | g -v '^.' | g TODO"
alias uuid="python -c 'import uuid; print(uuid.uuid4())'"
alias vrc="go nvim; vi ."
alias zzz='echo Shutdown in 10 seconds...; sleep 10; shutdown 0'
alias findnoext='find . -type f -regextype posix-extended -not -regex ".*\\.\w+$" -not -path "*/\.*"'

# apt
alias apti='sudo apt-get install --upgrade'
alias aptr='sudo apt-get autoremove'
alias apts='apt list | sort -f | grep -Pi'
alias aptS='apt show'
alias aptu='sudo apt-get remove --purge'
alias aptup='sudo apt-get update && sudo apt-get upgrade'
alias aptU='sudo apt-get update'

# grep
alias grep='rg'
alias ag='alias | sort -f | grep -Pi'
alias g='sort -f | grep -Pi'
alias gn='grep -Pni'
alias h='history'
alias hg='history | grep -Pi'
alias lg="ls -A --group-directories-first | grep -v '^./.(venv|git|mypy)|__pycache__' | sort -f -k 9 | grep -Pi"
alias lgr="find -L | grep -v '^./.(venv|git|mypy)|__pycache__' | grep -Pi"
alias pg="ps aux | g"


# youtube-dl
ytp(){
	yt-dlp -j --flat-playlist "$1" | jq -r '"- [" + .uploader + " - " + .title + "](" + .url + ")"'
}

yt(){
	if [ "$#" -eq 0 ]; then
		echo "[ERROR] Use: yt <URL> [<URL> ...]"
    elif [ "$#" -eq 1 ]; then
		_CWD="$(pwd)"
		set -f  # Disable globbing
		IFS=$'\n'  # Set IFS to newline

		cd "$HOME/brain/links/"
		yt-dlp --flat-playlist -j "$1" > "yt.json"
		echo "[" $(cat "yt.json" | sed '$!s/$/,/') "]" > "yt.json"

		_dest="yt_"$(echo "$1" | grep -oP 'youtube.com/@\K[^/]+')".md"
		rm -f "$_dest" > /dev/null
		cat "yt.json" | jq -r '.[] | "- [\(.playlist_uploader) - \(.title)](\(.url))"' | while read -r line; do echo "$line" >> "$_dest"; done
		sort "$(pwd)/$_dest" -o "$(pwd)/$_dest"
		echo "Saved: $(pwd)/$_dest"

		rm -f "yt.json" > /dev/null
		set +f  # Re-enable globbing
		cd $_CWD
	else
        for url in "$@"; do yt "$url" ; done
	fi
}

dla() {
	if (($#)); then
		python "$HOME/scripts/ytdl.py" audio $@
	fi
}

dlv() {
	if (($#)); then
		python "$HOME/scripts/ytdl.py" video $@
	fi
}

p() {
	cd "$_DATA/downloads/0_podcasts"
	if (($#)); then
		python "$HOME/scripts/ytdl.py" podcast $@
	fi
}

m() {
	cd $HOME/music/0_NEW
	if (($#)); then
		python "$HOME/scripts/ytdl.py" music $@
	fi
}

v() {
	cd "$_DATA/downloads/0_videos"
	if (($#)); then
		python "$HOME/scripts/ytdl.py" video $@
	fi
}

extract(){
	if [[ "$1" == *.zip ]]; then
		unzip "$1"
	elif [[ "$1" == *.rar ]]; then
		unrar x "$1"
	elif [[ "$1" == *.tar ]]; then
		tar -xvf "$1"
	elif [[ "$1" == *.tar.xz ]]; then
		tar -xvf "$1"
	elif [[ "$1" == *.tar.gz || "$1" == *.tgz ]]; then
		tar -xzvf "$1"
	elif [[ "$1" == *.gz ]]; then
		gzip -dk "$1"
	elif [[ "$1" == *.tar.bz2 ]]; then
		tar -xjvf "$1"
	elif [[ "$1" == *.bz2  ]]; then
		bzip2 -d "$1"
	else
		echo "[ERROR] Can't extract from that filetype"
	fi
}


archive(){
	fn="${1##*/}"
	fn="${fn%%.tar.bz2}"
	fn="${fn%%.tar.gz}"
	fn="${fn%%.tar.xz}"
	fn="${fn%%.tgz}"
	fn="${fn%%.bz2}"
	fn="${fn%%.gz}"
	fn="${fn%%.zip}"

	if [[ $# -eq 0 ]]; then
		echo "[ERROR] No argument given"
	elif ! [[ -e "$fn" || -d "$fn" || -e "$1" || -d "$1" ]]; then
		echo "[ERROR] $1 doesn't exist"
	elif [[ "$1" == */ ]]; then
		# no extension given
		fn=$(basename "$1")
		tar -czvf "$fn.tgz" "$fn"
	elif [[ "$fn" == "$1" ]]; then
		# no extension given
		tar -czvf "$1.tgz" "$1"
	elif [[ "$1" == *.tar.gz || "$1" == *.tgz || "$1" == *.gz ]]; then
		tar -czvf "$1" "$fn"
	elif [[ "$1" == *.tar.bz2 || "$1" == *.bz2 ]]; then
		tar -cjvf "$1" "$fn"
	elif [[ "$1" == *.tar.xz ]]; then
		tar -cJvf "$1" "$fn"
	elif [[ "$1" == *.zip ]]; then
		zip "$1" -r "$fn"
	else
		echo "[ERROR] Can't archive to that filetype"
	fi
}

exectime(){
	start=$(date +%s.%N)
	for i in $(seq $1); do "${@:2}"; done
	end=$(date +%s.%N)
	echo $end - $start | bc
}

times(){
	modified_timestamp=$(stat -c "%Y" "$1")
	created_timestamp=$(stat -c "%W" "$1")
	echo "Created: $(date -d @"$created_timestamp" '+%Y-%m-%d %H:%M:%S')"
	echo "Modified: $(date -d @"$modified_timestamp" '+%Y-%m-%d %H:%M:%S')"
}


map() {
	python "$_DATA/system/scripts/map.py" $@
}

mkcd() {
	mkdir "$@" && cd "$@"
}

stdfn(){
	# DIRS -----------------------------------------
	# we do dirs first to not get false positives while searching the files
	# for dirs we have to repeat each step until no more renames happen
	# this is because we can only rename the dir at the deepest level

	declare ignore=()
	# TODO: convert to python
	# TODO: special stdfn for some folders
		- e.g. for music: {artist 1, artist 2} - {title}
		- e.g. for books: {author 1, author 2} - {title}
		- e.g. for people: {firstname lastname}

	## make lowercase
	flag=1
	while [ $flag -gt 0 ]; do
		find . -type d -regextype posix-egrep \
			-not -regex '.*\/\..*' \
			-regex '.*[A-Z].*' \
			-exec rename -v 's/([A-Z]+)/\L$1/g' {} \;

		flag=$?
	done

	## remove quotes
	flag=1
	while [ $flag -gt 0 ]; do
		find . -type d -regextype posix-egrep \
			-not -regex '.*\/\..*' \
			-regex '.*".*' \
			-or -regex ".*'.*" \
			-exec rename -v 's/"//g' {} \; \
			-exec rename -v "s/'//g" {} \;

		flag=$?
	done

	## space or semicolon -> underscore
	flag=1
	while [ $flag -gt 0 ]; do
		find . -type d -regextype posix-egrep \
			-not -regex '.*\/\..*' \
			-regex '.*(:| )[^\/]*$' \
			-exec rename -v 's/(:| )(?!\/)/_/g' {} \;

		flag=$?
	done

	## dash -> underscore
	flag=1
	while [ $flag -gt 0 ]; do
		find . -type d -regextype posix-egrep \
			-not -regex '.*\/\..*' \
			-regex '.*-.*' \
			-not -regex '.*[0-9]{4}-[0-9]{2}-[0-9]{2}(_[0-9]{2}\.[0-9]{2}\.[0-9]{2})?_?[^_]*$' \
			-exec rename -v 's/(?<![0-9])-(?![0-9])$/_/g' {} \;

		flag=$?
	done

	## many underscores -> underscore
	flag=1
	while [ $flag -gt 0 ]; do
		find . -type d -regextype posix-egrep \
			-not -regex '.*\/\..*' \
			-regex '.*__.*' \
			-exec rename -v 's/_{2,}(?!__)/_/g' {} \;

		flag=$?
	done

	## date -> yyyy-mm-dd
	flag=1
	while [ $flag -gt 0 ]; do
		find . -type d -regextype posix-egrep \
			-not -regex '.*\/\..*' \
			-regex '.*[0-9]{4}_[0-9]{2}_[0-9]{2}.*' \
			-not -regex '.*[0-9]{4}-[0-9]{2}-[0-9]{2}(_[0-9]{2}\.[0-9]{2}\.[0-9]{2})?_?[^_]*$' \
			-exec rename -v 's/(?<!^)([0-9]{4})[-_]([0-9]{2})[-_]([0-9]{2})/$1-$2-$3/g' {} \;

		flag=$?
	done

	## datetime -> yyyy-mm-dd_HH.MM.DD
	flag=1
	while [ $flag -gt 0 ]; do
		find . -type d -regextype posix-egrep \
			-not -regex '.*\/\..*' \
			-regex '.*[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{2}.*' \
			-not -regex '.*[0-9]{4}-[0-9]{2}-[0-9]{2}(_[0-9]{2}\.[0-9]{2}\.[0-9]{2})?_?[^_]*$' \
			-exec rename -v 's/(?<!^)([0-9]{4})-([0-9]{2})-([0-9]{2})_([0-9]{2})_([0-9]{2})_([0-9]{2})/$1-$2-$3_$4.$5.$6/g' {} \;

		flag=$?
	done

	# FILES ----------------------------------------
	## make lowercase
	find . -type f -regextype posix-egrep \
		-not -regex '.*\/\..*' \
		-regex '.*[A-Z].*' \
		-exec rename -v 's/([A-Z]+)/\L$1/g' {} \;

	## txt -> markdown
	find . -type f -regextype posix-egrep \
		-not -regex '.*\/\..*' \
		-regex '.*\.txt.*' \
		-exec rename -v 's/\.txt/.md/g' {} \;

	## remove quotes
	find . -type d -regextype posix-egrep \
		-not -regex '.*\/\..*' \
		-regex '.*".*' \
		-or -regex ".*'.*" \
		-exec rename -v 's/"//g' {} \; \
		-exec rename -v "s/'//g" {} \;

	## space or semicolon -> underscore
	find . -type f -regextype posix-egrep \
		-not -regex '.*\/\..*' \
		-regex '.*(:| )[^\/]*$' \
		-exec rename -v 's/(:| )/_/g' {} \;

	## dash -> underscore
	find . -type f -regextype posix-egrep \
		-not -regex '.*\/\..*' \
		-regex '.*-[^\/]*' \
		-not -regex '.*[0-9]{4}-[0-9]{2}-[0-9]{2}(_[0-9]{2}\.[0-9]{2}\.[0-9]{2})?[^-]*$' \
		-exec rename -v 's/([^0-9])-/$1_/g' {} \; \
		-exec rename -v 's/-([^0-9])/_$1/g' {} \;

	## many underscores -> underscore
	find . -type f -regextype posix-egrep \
		-not -regex '.*\/\..*' \
		-regex '.*__.*' \
		-exec rename -v 's/_{2,}/_/g' {} \;

	## date -> yyyy-mm-dd
	find . -type f -regextype posix-egrep \
		-not -regex '.*\/\..*' \
		-regex '.*[0-9]{4}_[0-9]{2}_[0-9]{2}[^\/]*' \
		-not -regex '.*[0-9]{4}-[0-9]{2}-[0-9]{2}(_[0-9]{2}\.[0-9]{2}\.[0-9]{2})?_?[^_]*$' \
		-exec rename -v 's/(?<!^)([0-9]{4})[-_]([0-9]{2})[-_]([0-9]{2})/$1-$2-$3/g' {} \;

	## datetime -> yyyy-mm-dd_HH.MM.DD
	find . -type f -regextype posix-egrep \
		-not -regex '.*\/\..*' \
		-regex '.*[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{2}[^\/]*' \
		-not -regex '.*[0-9]{4}-[0-9]{2}-[0-9]{2}(_[0-9]{2}\.[0-9]{2}\.[0-9]{2})?_?[^_]*$' \
		-exec rename -v 's/(?<!^)([0-9]{4})-([0-9]{2})-([0-9]{2})[-_: ]([0-9]{2})[-_: ]([0-9]{2})[-_: ]([0-9]{2})/$1-$2-$3_$4.$5.$6/g' {} \;
}

dt(){
	if (($#)); then
		day=$(date -d "+$@ days" "+%Y_%m_%d")
	else
		day=$(date +%Y_%m_%d)
	fi

	echo $day | xclip -selection clipboard && xclip -o -selection clipboard
}

T(){
	if [[ $THEME -eq 1 ]]; then
		THEME=2
	else
		THEME=1
	fi
	xdotool key Menu Alt+t r $THEME
}


trade() {
	python "$_DATA/system/scripts/trading_calculator.py" $@
}

c() {
	python "$_DATA/system/scripts/price_split.py" $@
}

left() {
	if (($#)); then
		python "$_DATA/system/scripts/align.py" left $@ | xclip -selection clipboard && xclip -o -selection clipboard
	fi
}

right() {
	if (($#)); then
		python "$_DATA/system/scripts/align.py" right $@ | xclip -selection clipboard && xclip -o -selection clipboard
	fi
}

center() {
	if (($#)); then
		python "$_DATA/system/scripts/align.py" center $@ | xclip -selection clipboard && xclip -o -selection clipboard
	fi
}

copyclip(){
	$@ | xclip -selection clipboard && xclip -o -selection clipboard
}

copy(){
	if [[ $# -ne 3 ]]; then
		echo '[ERROR] Use: copy <n> <src> <dest>'
		return
	fi

	n=$1
	src=$2
	dest=$3
	find "$src" -maxdepth 1 | tail -n +2 | head -n $n | xargs -I {} cp {} "$dest"
}

copy_to_dirs() {
	if [[ $# -lt 2 ]]; then
		echo '[ERROR] Use: copy_to_dirs <src> <dest1> [<dest2> ...]'
		return 1
	fi

	src=$1
	shift
	for dest in "$@"; do
		cp "$src" "$dest" || {
			echo "[ERROR] Failed to copy to $dest"
			return 1
		}
	done
}

move(){
	if [[ $# -ne 3 ]]; then
		echo '[ERROR] Use: move <n> <src> <dest>'
		return
	fi

	n=$1
	src=$2
	dest=$3
    find "$src" -maxdepth 1 -print0 | tail -n +2 -z | head -n "$n" -z | xargs -0 -I {} mv {} "$dest"
}

oa(){
	if (($#)); then
		find $1 -type f | head -n 1 | xargs xdg-open
	else
		echo '[ERROR] Use: oa <path>'
		return
	fi
}

of() {
    dir="${1:-.}"
    find "$dir" -type f | sort | head -n 1 | while IFS= read -r file; do xdg-open "$file"; done
}

touch(){
	if [[ -e "$1" ]]; then
		echo "[ERROR] $1 already exists"
		return 1
	fi

	# accumulate the options before creating the files
	# just in case some of the options are at the end
	options=""
	for arg in "$@"; do
	   if [[ "$arg" == -* ]]; then
		   options="$options $arg"
		fi
	done

	for arg in "$@"; do
		if [[ "$arg" == -* ]]; then
			continue
		fi

		file_extension=".${arg##*.}"
		template_path="${templates[$file_extension]}"

		if [[ -n "$template_path" ]] && [[ -f "$template_path" ]]; then
			command cp "$template_path" "$arg"
		else
			command touch "$arg"
		fi
	done
}

check(){
	if [[ $# -ne 2 ]]; then
		echo '[ERROR] Use: check <file> <hash> or check <hash> <file>'
		return
	fi

	if [[ -f "$1" ]]; then
		f="$1"
		hash="$2"
	else
		f="$2"
		hash="$1"
	fi
	echo "$hash $f" | sha256sum -c -
}

swap(){
	sudo swapoff -a
	sudo swapon -a
}


rename_to_numbers(){
    dir="${1:-$(pwd)}"
    ~/scripts/.venv/bin/python3 ~/scripts/rename_to_numbers.py "$dir"
}

fix_nvim() {
    find ~/.local/share/nvim -mindepth 1 -maxdepth 1 ! -name 'harpoon.json' -exec rm -rf {} +
}

set +a  # disable automatic export after this line
