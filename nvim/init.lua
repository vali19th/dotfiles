require("config.set")
require("config.utils")
require("config.keymap")
require("config.statusline")
require("config.plugin_manager")

require("config.jupyter")
require("config.markdown")

-------------------------------------------------- COMMANDS --------------------
-- Fix a bug that generates lots of annoying errors
-- The bug happens when treesitter interacts with nvim-cmp or my "q/i\\v" keymap
vim.cmd [[autocmd TextChangedI * lua vim.schedule(function() end, 0)]]

-------------------------------------------------- EDIT ------------------------
-- don't create comments on new lines
vim.cmd [[autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o]]

-- highlight yanked text
vim.cmd [[autocmd TextYankPost * silent! lua vim.highlight.on_yank {higroup="IncSearch", timeout=150}]]

-------------------------------------------------- ON LOAD ---------------------
vim.cmd [[autocmd BufRead,BufNewFile *.md nnoremap <buffer> gf :lua _G.open_markdown_link()<CR>]]
vim.cmd [[autocmd BufRead,BufNewFile *.md syn match markdownError "\w\@<=\w\@="]]

vim.cmd [[autocmd BufRead,BufNewFile *bashrc* set filetype=sh]]
vim.cmd [[autocmd FileType htmldjango set filetype=html]]

-------------------------------------------------- ON SAVE ---------------------
vim.cmd [[autocmd BufWritePre *.lua,*.go,*.sql,*.html,*.css,*.js lua vim.lsp.buf.format()]]
vim.cmd [[autocmd BufWritePre *.py lua _G.format_with_black()]]
vim.cmd [[autocmd BufWritePost *.py silent !python -m jupyter_ascending.requests.sync --filename %:p]]

vim.cmd [[autocmd BufWritePre * %s/\v\s+$//e]]
vim.cmd [[autocmd BufWritePre * lua _G.remove_empty_lines_at_EOF()]]

-------------------------------------------------- MACROS ---------------------
local macro_path = vim.fn.expand("~/.local/share/nvim/macros.txt")

-- Save macros on exit
vim.api.nvim_create_autocmd("VimLeave", {
    callback = function()
        local macro_lines = {}
        for _, line in ipairs(vim.split(vim.fn.execute("reg"), "\n")) do
            -- Filter lines that define registers (skip headers)
            local reg, content = line:match('^"([a-zA-Z0-9])%s+(.*)$')
            if reg and content then
                -- Format as a valid Vimscript command
                table.insert(macro_lines, string.format('let @%s = %q', reg, content))
            end
        end
        vim.fn.writefile(macro_lines, macro_path)
    end,
})

-- Load macros on startup
vim.api.nvim_create_autocmd("VimEnter", {
    callback = function()
        if vim.fn.filereadable(macro_path) == 1 then
            vim.cmd("source " .. macro_path)
        end
    end,
})

--[[
- on startup:
    - async check if
        1. 5m since last update
        2. or new commit hash
        3. or file.json.tmp exists
    - on commit: look only at the new/updated/DELETED files
- on BufWritePost
- init
    - if json missing, check all files
- on load error, delete and rebuild

- extraction
    - find yaml if it exists
    - parse yaml
    - find "aliases" section
    - get all items from different formats
        ```yaml
        aliases: a
        aliases: "a"
        aliases: ["a", "b"]
        aliases:
            - a
            - b
        ```
    - reload json (unless I can reload it on startup and keep it in memory)
    - update json
    - save json
    - use 2 data structures:
        - {"alias": [f1, f2, ...]
        - {"file": {last_edit: ..., aliases: [a, b, ...]}
    - use atomic save (save to .tmp, then rename)
]] --
