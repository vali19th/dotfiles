function _G.press(keys, mode)
    mode = mode or "n"
    vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(keys, true, false, true), mode, false)
end

function _G.fix_spelling()
    -- vim.fn.spellbadword() moves to the next bad word if the current one is ok, so we have to keep track of 3 positions
    -- pos_0 - the initial position
    -- pos_1 - after we move to the beginning of the current word with "lb"
    -- pos_2 - the position after the vim.fn.spellbadword() call

    -- if pos_1 != pos_2 or not a bad word, it means the current word is ok. Move back to the initial position and exit
    -- else try replacing it with the first suggestion in the list and move to the same column as before if you were not at the beginning of the word
    -- if there's no suggestion, do nothing

    local pos_0 = vim.api.nvim_win_get_cursor(0)
    vim.cmd('normal! l')
    vim.cmd('normal! b')
    local pos_1 = vim.api.nvim_win_get_cursor(0)
    local bad_word = vim.fn.spellbadword()[1]
    local pos_2 = vim.api.nvim_win_get_cursor(0)

    if (pos_1[1] ~= pos_2[1]) or (pos_1[2] ~= pos_2[2]) or bad_word == "" then
        vim.api.nvim_win_set_cursor(0, pos_0)
        return
    end

    local suggestions = vim.fn.spellsuggest(vim.fn.expand('<cword>'))
    if #suggestions > 0 then
        vim.cmd('normal! ciw' .. suggestions[1])
        vim.api.nvim_win_set_cursor(0, pos_0)
    else
        print("No suggestions available")
    end
end

function _G.fix_spelling_and_jump()
    _G.fix_spelling()
    _G.press("]s")
end

function _G.get_next_day(date)
    local year, month, day = date:match("(%d%d%d%d)-(%d%d)-(%d%d)")
    return os.date("%Y-%m-%d", os.time { year = year, month = month, day = day + 1 })
end

function _G.get_prev_day(date)
    local year, month, day = date:match("(%d%d%d%d)-(%d%d)-(%d%d)")
    return os.date("%Y-%m-%d", os.time { year = year, month = month, day = day - 1 })
end

function _G.format_with_black()
    vim.cmd("write")
    local file = vim.fn.expand('%:p')
    if _G.skip_format(file) then
        return
    end

    vim.cmd("silent !autopep8  --max-line-length 120 --aggressive --aggressive --in-place " .. file .. " > /dev/null")
    if vim.v.shell_error > 0 then
        vim.defer_fn(function() print("ERROR formatting with autopep8") end, 10) -- Delay in milliseconds
        return
    end

    vim.cmd("silent !black --line-length 120 " .. file .. " > /dev/null")
    if vim.v.shell_error > 0 then
        vim.defer_fn(function() print("ERROR formatting with black") end, 10) -- Delay in milliseconds
        return
    end

    local handle = io.popen("tidy-imports --black --quiet --replace-star-imports --action REPLACE " .. file .. " 2>&1")
    if handle then
        local result = handle:read("*a")
        handle:close()
        if result and string.find(result, "PYFLYBY") then
            print(result)
        end
    end

    -- this has to be after pyflyby, so it can fix the grouping and ordering
    vim.cmd("silent !isort --profile black " .. file .. " > /dev/null")
    if vim.v.shell_error > 0 then
        vim.defer_fn(function() print("ERROR formatting with isort") end, 10) -- Delay in milliseconds
        return
    end

    vim.cmd("edit!")
end

function _G.skip_format(path)
    if not path:match('/cubicustomizations/') then
        return false
    end

    local allowed_files = {
        'bug_prolonging_walls.py',
        'build_fuzzy_cache.py',
        'corel_masks_with_ids.py',
        'extract_distribution.py',
        'find_room_pairing_distribution.py',
        'find_surface_heuristics.py',
        'main_crop.py',
        'metadata.py',
        'monitor_hardware.py',
        'new_production.py',
        'ocr_evaluation.py',
        'ocr_pseudolabeling.py',
        'outer_walls.py',
        'p_diff_fuzzy.py',
        'poc_find_minmax_thresholds_for_surfaces.py',
        'poc_whitelist_room_merges.py',
        'prepare_OCR_csv.py',
        'profile_imports.py',
        'quick_ocr.py',
        'room_allocation.py',
        'sandbox.py',
        'sandbox_OCR_integration.py',
        'sandbox_scale.py',
        'svg_to_blender.py',
        'tests/test_data.py',
        'tests/test_fuzzy.py',
        'tests/test_geometry.py',
        'tests/test_ocr.py',
        'tests/test_room_allocation.py',
        'utils/custom.py',
        'utils/data.py',
        'utils/data_structures.py',
        'utils/fuzzy.py',
        'utils/magic.py',
        'utils/monitoring.py',
        'utils/ocr_utils.py',
        'utils/sqlite.py',
        'utils/sturdy_geometry_utils.py',
        'utils/svg_utils.py',
        'utils/sys_utils.py',
    }

    for _, allowed_file in ipairs(allowed_files) do
        if path:match(allowed_file) then
            return false
        end
    end

    return true
end

function _G.toggle_terminal()
    local term_buf_nr = vim.g.term_buf_nr

    -- if the terminal buffer exists and is valid
    if term_buf_nr and vim.api.nvim_buf_is_valid(term_buf_nr) then
        -- If it exists, switch to it in the current window
        vim.api.nvim_set_current_buf(term_buf_nr)
    else
        -- Else, create a new terminal and enter insert mode
        vim.cmd("terminal")
        vim.g.term_buf_nr = vim.api.nvim_get_current_buf() -- save the buffer number
        vim.cmd("startinsert")
    end
end

function _G.remove_empty_lines_at_EOF()
    local lines = vim.api.nvim_buf_get_lines(0, 0, -1, false)

    local last_non_empty = #lines
    while last_non_empty > 0 and lines[last_non_empty] == "" do
        last_non_empty = last_non_empty - 1
    end

    vim.api.nvim_buf_set_lines(0, last_non_empty, -1, false, {})
end

function _G.close_buffer_or_exit()
    local current_buffer = vim.api.nvim_get_current_buf()

    if vim.api.nvim_buf_get_option(current_buffer, 'modified') then
        -- The buffer is modified, ask for confirmation
        local confirm = vim.fn.confirm("The buffer is modified. Quit anyway?", "&Yes\n&No", 1)
        if confirm ~= 1 then
            return
        end
    end

    vim.api.nvim_buf_delete(current_buffer, { force = true })

    local open_buffers = 0
    for _, buf in ipairs(vim.api.nvim_list_bufs()) do
        if vim.api.nvim_buf_is_loaded(buf) and vim.api.nvim_buf_get_option(buf, 'buflisted') then
            local buftype = vim.api.nvim_buf_get_option(buf, 'buftype')
            local name = vim.api.nvim_buf_get_name(buf)
            if (buftype == '' or buftype == 'acwrite') and (name ~= '' or vim.api.nvim_buf_get_option(buf, 'modified')) then
                open_buffers = open_buffers + 1
            end
        end
    end

    if open_buffers == 0 then
        vim.cmd('quit')
    end
end

function _G.reload_config()
    -- clear cache
    for name, _ in pairs(package.loaded) do
        if name:match("^config") or name:match("^after/plugin") then
            package.loaded[name] = nil
        end
    end

    -- reload
    dofile(vim.env.HOME .. '/.config/nvim/init.lua')
    local after_plugin_path = vim.env.HOME .. '/.config/nvim/after/plugin/'
    for _, plugin_file in ipairs(vim.fn.globpath(after_plugin_path, "*.lua", false, true)) do
        dofile(plugin_file)
    end
end

function _G.title_case(str)
    -- stop words will be lowercase
    local stop_words = {
        a = true,
        of = true,
        the = true
    }

    return str:gsub("(%a[%w_']*)", function(word)
        local lower_word = word:lower()
        if stop_words[lower_word] then
            return lower_word
        elseif word == word:upper() then
            return word
        else
            return word:sub(1, 1):upper() .. word:sub(2):lower()
        end
    end)
end

function _G.reverse_table(tbl)
    local reversed = {}
    for i = #tbl, 1, -1 do
        table.insert(reversed, tbl[i])
    end
    return reversed
end
