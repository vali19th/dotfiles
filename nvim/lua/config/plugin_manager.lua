local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end

vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    spec = "config.plugins",
    change_detection = { notify = false },
})

vim.api.nvim_set_keymap("n", "<leader>~", ":Lazy sync<CR>", { noremap = true, silent = true })

-- -- -- -- -- [[ Configure Treesitter ]]
-- -- -- -- -- See `:help nvim-treesitter`
-- -- -- -- -- Defer Treesitter setup after first render to improve startup time of 'nvim {filename}'
-- -- -- -- vim.defer_fn(function()
-- -- -- --   require('nvim-treesitter.configs').setup {
-- -- -- --     -- Add languages to be installed here that you want installed for treesitter
-- -- -- --     ensure_installed = { 'c', 'cpp', 'go', 'lua', 'python', 'rust', 'tsx', 'javascript', 'typescript', 'vimdoc', 'vim', 'bash' },

-- -- -- --     -- Autoinstall languages that are not installed. Defaults to false (but you can change for yourself!)
-- -- -- --     auto_install = false,

-- -- -- --     highlight = { enable = true },
-- -- -- --     indent = { enable = true },
-- -- -- --     incremental_selection = {
-- -- -- --       enable = true,
-- -- -- --       keymaps = {
-- -- -- --         init_selection = '<c-space>',
-- -- -- --         node_incremental = '<c-space>',
-- -- -- --         scope_incremental = '<c-s>',
-- -- -- --         node_decremental = '<M-space>',
-- -- -- --       },
-- -- -- --     },
-- -- -- --     textobjects = {
-- -- -- --       select = {
-- -- -- --         enable = true,
-- -- -- --         lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
-- -- -- --         keymaps = {
-- -- -- --           -- You can use the capture groups defined in textobjects.scm
-- -- -- --           ['aa'] = '@parameter.outer',
-- -- -- --           ['ia'] = '@parameter.inner',
-- -- -- --           ['af'] = '@function.outer',
-- -- -- --           ['if'] = '@function.inner',
-- -- -- --           ['ac'] = '@class.outer',
-- -- -- --           ['ic'] = '@class.inner',
-- -- -- --         },
-- -- -- --       },
-- -- -- --       move = {
-- -- -- --         enable = true,
-- -- -- --         set_jumps = true, -- whether to set jumps in the jumplist
-- -- -- --         goto_next_start = {
-- -- -- --           [']m'] = '@function.outer',
-- -- -- --           [']]'] = '@class.outer',
-- -- -- --         },
-- -- -- --         goto_next_end = {
-- -- -- --           [']M'] = '@function.outer',
-- -- -- --           [']['] = '@class.outer',
-- -- -- --         },
-- -- -- --         goto_previous_start = {
-- -- -- --           ['[m'] = '@function.outer',
-- -- -- --           ['[['] = '@class.outer',
-- -- -- --         },
-- -- -- --         goto_previous_end = {
-- -- -- --           ['[M'] = '@function.outer',
-- -- -- --           ['[]'] = '@class.outer',
-- -- -- --         },
-- -- -- --       },
-- -- -- --       swap = {
-- -- -- --         enable = true,
-- -- -- --         swap_next = {
-- -- -- --           ['<leader>a'] = '@parameter.inner',
-- -- -- --         },
-- -- -- --         swap_previous = {
-- -- -- --           ['<leader>A'] = '@parameter.inner',
-- -- -- --         },
-- -- -- --       },
-- -- -- --     },
-- -- -- --   }
-- -- -- -- end, 0)

-- -- -- -- -- [[ Configure LSP ]]
-- -- -- -- --  This function gets run when an LSP connects to a particular buffer.
-- -- -- -- local on_attach = function(_, bufnr)
-- -- -- --   -- NOTE: Remember that lua is a real programming language, and as such it is possible
-- -- -- --   -- to define small helper and utility functions so you don't have to repeat yourself
-- -- -- --   -- many times.
-- -- -- --   --
-- -- -- --   -- In this case, we create a function that lets us more easily define mappings specific
-- -- -- --   -- for LSP related items. It sets the mode, buffer and description for us each time.
-- -- -- --   local nmap = function(keys, func, desc)
-- -- -- --     if desc then
-- -- -- --       desc = 'LSP: ' .. desc
-- -- -- --     end

-- -- -- --     vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
-- -- -- --   end

-- -- -- --   nmap('<leader>rn', vim.lsp.buf.rename, '[R]e[n]ame')
-- -- -- --   nmap('<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')

-- -- -- --   vim.keymap.set("n", "gs", function() vim.lsp.buf.document_symbol() end, opts)
-- -- -- --   nmap('gI', require('telescope.builtin').lsp_implementations, '[G]oto [I]mplementation')
-- -- -- --   nmap('<leader>D', require('telescope.builtin').lsp_type_definitions, 'Type [D]efinition')
-- -- -- --   -- See `:help K` for why this keymap
-- -- -- --   nmap('K', vim.lsp.buf.hover, 'Hover Documentation')
-- -- -- --   nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')

-- -- -- --   -- Lesser used LSP functionality
-- -- -- --   nmap('gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')
