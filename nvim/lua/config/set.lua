vim.fn.setenv("PATH", vim.fn.getenv("PATH") .. ":" .. vim.fn.expand("~/.venv/bin"))
vim.g.python3_host_prog = "~/.venv/bin/python"
vim.opt.clipboard = "unnamedplus" -- use system clipboard for copy/paste
vim.opt.fileformat = "unix"

-- UI --------------------------------------------------------------------------
vim.g.netrw_list_hide = "^\\(\\.git\\/\\|\\.venv\\/\\)$"
vim.g.netrw_liststyle = 3 -- tree-style file listing
vim.opt.colorcolumn = "120"
vim.opt.cursorline = true -- highlight current line
vim.opt.mouse = "a"
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.showmatch = true   -- show matching brackets
vim.opt.signcolumn = "yes" -- always show sign column
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.termguicolors = true -- enable 24bit RGB colors
vim.opt.title = true         -- show the file name in terminal title

-- editor ----------------------------------------------------------------------
vim.opt.backspace = { "indent", "eol", "start" } -- allow backspacing over everything in insert mode
vim.opt.history = 10000
vim.opt.linebreak = true                         -- wrap lines at word boundaries
vim.opt.spell = true
vim.opt.spelllang = { "en", "ro" }
vim.opt.spellsuggest = "best"
vim.opt.spellcapcheck = ""
vim.opt.updatetime = 50 -- update swap files every 50ms


-- folding ---------------------------------------------------------------------
vim.opt.foldenable = true
vim.opt.foldlevel = 1
vim.opt.foldmethod = "manual"

-- tabs / indentation ----------------------------------------------------------
vim.opt.expandtab = true
vim.opt.smartindent = true -- auto indent new lines
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.tabstop = 4

-- search ----------------------------------------------------------------------
vim.opt.hlsearch = true   -- highlight search results
vim.opt.ignorecase = true -- ignore case when searching
vim.opt.incsearch = true  -- show search matches as you type
vim.opt.smartcase = true  -- ignore case if search pattern is all lowercase

-- offset N lines above and below the cursor -----------------------------------
vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

-- disabled so you can use "undotree" ------------------------------------------
vim.opt.backup = false
vim.opt.swapfile = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true -- enable persistent undo
