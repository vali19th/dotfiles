local function extract_markdown_link()
    local line = vim.fn.getline('.')
    local col = tonumber(vim.fn.col('.'))
    local patterns_to_replace = {
        "#%^[^|]*|[^%]]*", -- [[file#^section|alias]]
        "#%^[^%]]*",       -- [[file#^section]]
        "|[^%]]*",         -- [[file|alias]]
    }

    for start_pos, match, end_pos in line:gmatch("()%[%[(.-)%]%]()") do
        start_pos = tonumber(start_pos)
        end_pos = tonumber(end_pos)

        if start_pos <= col and col <= end_pos then
            for _, pattern in ipairs(patterns_to_replace) do
                match = match:gsub(pattern, "")
            end
            return match
        end
    end
end

function _G.markdown_link_history()
    vim.cmd("silent! %s/\\v^\\d\\d:\\d\\d (AM|PM)//g")
    vim.cmd("silent! %s/ - YouTube$//g")
    vim.cmd("silent! %s/^\\(http[^&]*\\)&.*/\\1/g")
    vim.cmd("silent! %s/More From Site\\n//g")
    vim.cmd("silent! %s/Delete\\n//g")
    vim.cmd("silent! %s/\\n\\n\\+/\\r/g")
    vim.cmd('silent! 1s/^\\n//')

    local lines = vim.api.nvim_buf_get_lines(0, 0, -1, false)
    local transformed_lines = {}
    local date = os.date("%Y-%m-%d")

    for i = 1, #lines, 3 do
        if lines[i] and lines[i + 1] and lines[i + 2] then
            local hh, mm, ampm = lines[i]:match("(%d%d):(%d%d):%d%d (%a%a)")

            if hh and mm and ampm then
                if ampm == 'PM' and tonumber(hh) < 12 then
                    hh = tostring(tonumber(hh) + 12)
                elseif ampm == 'AM' and tonumber(hh) == 12 then
                    hh = '00'
                end

                local title = lines[i + 1]
                local link = lines[i + 2]

                table.insert(transformed_lines, string.format("- %s %s:%s [%s](%s)", date, hh, mm, title, link))
            end
        end
    end

    vim.api.nvim_buf_set_lines(0, 0, -1, false, transformed_lines)
end

function _G.open_markdown_link()
    local path = extract_markdown_link() or vim.fn.expand('<cfile>')
    local path_md = path .. '.md'

    local edit_path = 'edit ' .. path:gsub("%%", "\\%%")
    local edit_path_md = 'edit ' .. path_md:gsub("%%", "\\%%")

    if vim.fn.filereadable(path) == 1 then
        vim.cmd(edit_path)
    elseif vim.fn.filereadable(path_md) == 1 then
        vim.cmd(edit_path_md)
    else
        local confirm = vim.fn.confirm("'" .. path_md .. "' does not exist. Create it?", "&Yes\n&No", 1)
        if confirm == 1 then
            if path:match('myself/journal/') then
                local template = vim.fn.readfile('obsidian/templates/journal.md')
                vim.fn.writefile(template, path_md)

                local date = path:match('%d%d%d%d%-%d%d%-%d%d')
                local yesterday = 'myself/journal/' .. _G.get_prev_day(date)
                local tomorrow = 'myself/journal/' .. _G.get_next_day(date)
                local linked_days = string.format("[[%s|yesterday]] | [[%s|tomorrow]]", yesterday, tomorrow)

                vim.cmd(edit_path_md)
                vim.api.nvim_buf_set_lines(0, 0, 1, false, { linked_days })
                vim.cmd('silent write')
            else
                vim.cmd(edit_path_md)
            end
        else
            print('File not found: ' .. path)
        end
    end
end

local function prepare_structure(lines)
    local structure = {}
    local current_header = {}

    for i, line in ipairs(lines) do
        local level, header = line:match("^(#+) (.+)$")
        if level and header then
            while #current_header > #level do
                table.remove(current_header)
            end

            -- You've skipped at least one header level
            if #current_header < #level then
                table.insert(current_header, "?")
            end

            current_header[#level] = header
            table.insert(structure, { line = i, text = "#" .. #level .. " " .. table.concat(current_header, " -> ") })
        end
    end

    return _G.reverse_table(structure)
end

local function generate_structure_finder(lines)
    local entry_display = require("telescope.pickers.entry_display")
    return require("telescope.finders").new_table({
        results = prepare_structure(lines),
        entry_maker = function(entry)
            local displayer = entry_display.create({
                separator = " ",
                items = {
                    { width = 4 },
                    { remaining = true },
                },
            })

            local make_display = function()
                return displayer({ ("%4d"):format(entry.line), entry.text })
            end

            return {
                value = entry,
                display = make_display,
                ordinal = entry.text,
                lnum = entry.line,
            }
        end,
    })
end

function _G.markdown_structure()
    local current_buffer = vim.api.nvim_get_current_buf()
    local lines = vim.api.nvim_buf_get_lines(current_buffer, 0, -1, false)

    require("telescope.pickers").new({}, {
        prompt_title = "File Structure",
        finder = generate_structure_finder(lines),
        sorter = require("telescope.config").values.generic_sorter({}),
        attach_mappings = function(_, _)
            local actions = require("telescope.actions")
            actions.select_default:replace(function(prompt_bufnr)
                actions.close(prompt_bufnr)
                local selection = require("telescope.actions.state").get_selected_entry()
                if selection then
                    vim.api.nvim_win_set_cursor(0, { selection.lnum, 0 })
                    _G.press("zz")
                end
            end)
            return true
        end,
    }):find()
end

local function asciify(line)
    local replacements = {
        ['À'] = 'A',
        ['Á'] = 'A',
        ['Â'] = 'A',
        ['Ã'] = 'A',
        ['Ä'] = 'A',
        ['Ç'] = 'C',
        ['È'] = 'E',
        ['É'] = 'E',
        ['Ê'] = 'E',
        ['Ë'] = 'E',
        ['Ì'] = 'I',
        ['Í'] = 'I',
        ['Î'] = 'I',
        ['Ï'] = 'I',
        ['Ñ'] = 'N',
        ['Ò'] = 'O',
        ['Ó'] = 'O',
        ['Ô'] = 'O',
        ['Õ'] = 'O',
        ['Ö'] = 'O',
        ['Ù'] = 'U',
        ['Ú'] = 'U',
        ['Û'] = 'U',
        ['Ü'] = 'U',
        ['à'] = 'a',
        ['á'] = 'a',
        ['â'] = 'a',
        ['ã'] = 'a',
        ['ä'] = 'a',
        ['ç'] = 'c',
        ['è'] = 'e',
        ['é'] = 'e',
        ['ê'] = 'e',
        ['ë'] = 'e',
        ['ì'] = 'i',
        ['í'] = 'i',
        ['î'] = 'i',
        ['ï'] = 'i',
        ['ñ'] = 'n',
        ['ò'] = 'o',
        ['ó'] = 'o',
        ['ô'] = 'o',
        ['õ'] = 'o',
        ['ö'] = 'o',
        ['ù'] = 'u',
        ['ú'] = 'u',
        ['û'] = 'u',
        ['ü'] = 'u',
        ['Ă'] = 'A',
        ['ă'] = 'a',
        ['Ş'] = 'S',
        ['ş'] = 's',
        ['Ţ'] = 'T',
        ['ţ'] = 't',
        ['Ș'] = 'S',
        ['ș'] = 's',
        ['Ț'] = 'T',
        ['ț'] = 't',
        ['’'] = "'",
        ['‘'] = "'",
        ['“'] = '"',
        ['„'] = '"',
        ['”'] = '"',
        ['–'] = '-',
        ['—'] = '-',
        ['−'] = '-',
        ['¡'] = '!',
        ['¿'] = '?',
        ['？'] = '?',
        ['…'] = '...',
        ['•'] = '*',
        ['：'] = ':',
        ['＂'] = '"',
        ['｜'] = '|',
        ['!'] = '!',
        ['﹖'] = '?',
        ['→'] = '->',
    }

    line = line:gsub("[\192-\255][\128-\191]*", function(c)
        return replacements[c] or c
    end)

    local non_ascii_spaces = {
        "\u{00A0}", "\u{1680}", "\u{180E}", "\u{2000}", "\u{2001}", "\u{2002}", "\u{2003}", "\u{2004}", "\u{2005}",
        "\u{2006}", "\u{2007}", "\u{2008}", "\u{2009}", "\u{200A}", "\u{202F}", "\u{205F}", "\u{3000}", " "
    }
    line = line:gsub("[" .. table.concat(non_ascii_spaces, "") .. "]", " ")
    line = line:gsub("(%S)%s+", "%1 ") -- remove duplicate spaces

    local emoji_ranges = {
        "\u{1F600}-\u{1F64F}", "\u{1F300}-\u{1F5FF}", "\u{1F680}-\u{1F6FF}", "\u{1F700}-\u{1F77F}", "\u{1F780}-\u{1F7FF}",
        "\u{1F800}-\u{1F8FF}", "\u{1F900}-\u{1F9FF}", "\u{1FA00}-\u{1FA6F}", "\u{1FA70}-\u{1FAFF}", "\u{2600}-\u{26FF}",
        "\u{2700}-\u{27BF}", "\u{2300}-\u{23FF}", "\u{2B50}", "\u{1F004}", "\u{1F0CF}"
    }
    return line:gsub("[" .. table.concat(emoji_ranges, "") .. "]", "")
end

function _G.asciify_file()
    local lines = {}
    for _, line in ipairs(vim.api.nvim_buf_get_lines(0, 0, -1, false)) do
        line = asciify(line)
        table.insert(lines, line)
    end
    vim.api.nvim_buf_set_lines(0, 0, -1, false, lines)
end

function _G.standardise_file()
    local lines = {}
    local is_empty = false
    local inside_code_block = false
    local inside_yaml = nil

    for _, line in ipairs(vim.api.nvim_buf_get_lines(0, 0, -1, false)) do
        if line:match("^---$") then
            if inside_yaml == nil then
                inside_yaml = true
            elseif inside_yaml == false then
                inside_yaml = true
            elseif inside_yaml == true then
                inside_yaml = false
            end
            table.insert(lines, line) -- insert the block delimiter
            goto continue
        elseif inside_yaml then
            table.insert(lines, line)
            goto continue
        end

        if line:match("```") then
            inside_code_block = not inside_code_block
            table.insert(lines, line) -- insert the block delimiter
            goto continue
        end

        if inside_code_block then
            table.insert(lines, line)
            goto continue
        end

        if string.sub(line, 1, 1) == "|" then
            table.insert(lines, line)
            goto continue
        end

        is_empty = line:match("^%s*$") ~= nil
        if is_empty then
            goto continue
        end

        local indent, first_char, rest = line:match("^(%s*)(%S)(.*)$")
        if first_char:match("[^#0-9%-]") and not inside_code_block then
            line = indent .. "- " .. first_char .. rest
        end

        -- TODO: if Romanian is detected on a line, do not asciify Romanian letters, but asciify everything else on that line
        -- local function is_romanian_line(line)
        --     vim.opt.spelllang = "ro"
        --     for word in line:gmatch("%w+") do
        --         if vim.fn.spellbadword(word) == "" then -- is Romanian word
        --             vim.opt.spelllang = { "en", "ro" }
        --             return true
        --         end
        --     end
        --
        --     vim.opt.spelllang = { "en", "ro" }
        --     return false
        -- end

        line = asciify(line)
        line = line:gsub("\t", "    ")             -- replace tabs with spaces
        line = line:gsub("(%S)%s+", "%1 ")         -- remove duplicate spaces
        line = line:gsub(" %s*:", ":")             -- remove spaces before colon
        line = line:gsub("%[%s*(.-)%s*%]", "[%1]") -- remove spaces inside brackets
        line = line:gsub("%(%s*(.-)%s*%)", "(%1)") -- remove spaces inside parentheses
        line = line:gsub("(https://www.youtube.com/watch%?v=[%w_-]+)[&?][%w_=]*[%w_-]*", "%1")

        -- Capitalize new sentences
        line = line:gsub("([!?]) ([a-z])", function(p1, p2)
            return p1 .. " " .. p2:upper()
        end)
        line = line:gsub("([^g%d])%. ([a-z])", function(p1, p2)
            return p1 .. ". " .. p2:upper()
        end)
        line = line:gsub("([^%.])g%. ([a-z])", function(p1, p2)
            return p1 .. "g. " .. p2:upper()
        end)

        table.insert(lines, line)

        ::continue::
    end
    vim.api.nvim_buf_set_lines(0, 0, -1, false, lines)
end

local function html_decode(str)
    return (str:gsub("(&.-;)", {
        ["&amp;"] = "&",
        ["&lt;"] = "<",
        ["&gt;"] = ">",
        ["&quot;"] = "\"",
        ["&#39;"] = "'"
    }))
end

function _G.insert_link()
    local clipboard = vim.fn.getreg("+")
    local link = clipboard:match("https?://[,:%w%.%-_/%%?&=#@]+[,:%w%-_/%%?&=#]+") or ""
    local content = ""
    local title = ""

    if link:match("http") then
        local tmp = '/tmp/nvim_insert_link.html'
        os.execute(string.format("curl -sSL '%s' -o %s", link, tmp))
        local file = io.open(tmp, "r")

        if file then
            content = file:read("*all")
            file:close()

            title = content:match("<title>(.-)</title>") or ""
            title = html_decode(title)
            title = asciify(title)

            title = title:gsub("&#x27;", "'")
            title = title:gsub("^(.*) %- TradingView News$", "TradingView - %1")
            title = title:gsub("^(.*) %- (Real Python)$", "%2 - %1")
            title = title:gsub(" %- The Real Python Podcast$", "")
            title = title:gsub(" %- YouTube$", "") -- I will prepend "YouTube - " below
        end
    end

    if link:match("youtube%.com") or link:match("youtu%.be") then
        local channel = content:match('"channel"%s*:%s*{%"simpleText"%s*:%s*"(.-)"}')
        title = "YouTube - " .. channel .. " - " .. title

        link = link:gsub("?list=.*", "")
        link = link:gsub("&list=.*", "")
        link = link:gsub("&pp=.*", "")
    end

    -- convert unicode IDs to ASCII symbols
    title = title:gsub("\\u00(%x%x)", function(hex)
        return string.char(tonumber(hex, 16))
    end)

    title = asciify(title):match("^%s*(.-)%s*$")
    title = title:gsub("(%S)%s+", "%1 ") -- remove duplicate spaces
    vim.api.nvim_put({ "- [" .. title .. "](" .. link .. ")" }, "l", false, false)
    vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<Right><Right>", true, false, true), "n", false)
end

function _G.insert_playlist_links()
    local clipboard = vim.fn.getreg("*")
    local link = clipboard:match("https?://[%w%.%-_/%%?&=#@]+[%w%-_/%%?&=#]+") or ""
    link = link:match("list=([^&]+)")

    local cmd = 'yt-dlp -j --flat-playlist "' .. link .. '" 2>/dev/null '
    cmd = cmd .. '| jq -r \'"- [YouTube - \\(.uploader) - \\(.title)](\\(.url))"\''
    local handle = io.popen(cmd)
    local text = nil
    if handle ~= nil then
        text = handle:read("*a")
        handle:close()
    end

    local lines = {}
    for line in text:gmatch("([^\n]+)") do
        table.insert(lines, line)
    end

    vim.api.nvim_put(lines, "l", false, false)
end

function _G.get_project_dir()
    local cwd = vim.fn.getcwd()
    local dir = cwd
    while dir ~= "/" and vim.fn.isdirectory(dir .. "/.git") == 0 do
        dir = dir:gsub("/[^/]+$", "")
    end

    if dir == "/" then
        return cwd
    else
        return dir
    end
end

function _G.insert_file_link()
    local project_dir = _G.get_project_dir()
    local files = vim.fn.globpath(project_dir, "**", 0, 1)

    -- Filter out hidden files and dirs
    files = vim.tbl_filter(function(file)
        return not file:find("/%.")
    end, files)

    require('telescope.builtin').find_files({
        prompt_title = "Select File",
        cwd = project_dir,
        attach_mappings = function(_, map)
            map('i', '<CR>', function(bufnr)
                local selection = require('telescope.actions.state').get_selected_entry()
                require('telescope.actions').close(bufnr)
                if not selection then
                    return false
                end

                local path = selection.value:gsub("%.md$", "")
                local text = nil
                if #path:gsub("/[^/]+$", "") >= 2 then
                    local fn = path:gsub("^.*/", "")
                    text = "[[" .. path .. "|" .. fn .. "]]"
                else
                    text = "[[" .. path .. "]]"
                end

                local row, col = unpack(vim.api.nvim_win_get_cursor(0))
                vim.api.nvim_buf_set_text(0, row - 1, col, row - 1, col, { text })
                vim.api.nvim_win_set_cursor(0, { row, col + #text })
            end)
            return true
        end
    })
end

function _G.open_link()
    local url = vim.fn.expand('<cWORD>'):gsub("^.*%]%((.*)%).*$", "%1")
    if not url:match("https?://") then
        url = vim.api.nvim_get_current_line():match("%[.*%]%((.*)%)")
    end

    if url ~= nil and url:match("https?://") then
        os.execute('xdg-open "' .. url:gsub("'", "\\'") .. '" &')
    else
        print("Invalid URL")
        return
    end
end

vim.keymap.set("n", "gx", ":lua open_link()<CR>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>mA", ":lua asciify_file()<CR>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>ma", ":lua standardise_file()<CR>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>mf", ":lua insert_file_link()<CR>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>ml", ":lua insert_link()<CR>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>mL", ":lua insert_playlist_links()<CR>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>mp", ":%s/^/- <> <CR>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>ms", ":lua markdown_structure()<CR>", { noremap = true, silent = true })

-- vim.keymap.set("n", "<leader><leader>", ":source %<CR>", { noremap = true, silent = true })
