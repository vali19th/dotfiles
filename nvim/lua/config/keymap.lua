local function map(mode, lhs, rhs, opts)
    if opts["desc"] == nil then
        error("Description is missing for " .. lhs .. " -> " .. rhs)
    end

    local options = vim.tbl_extend("force",
        { silent = true, noremap = true }, -- defaults
        opts or {}                         -- overrides
    )

    vim.keymap.set(mode, lhs, rhs, options)
end

local function _gf()
    local filename = vim.fn.expand("<cfile>")

    -- First try: If it exists, open it
    if vim.fn.filereadable(filename) == 1 then
        vim.cmd("edit " .. filename)
        return
    end

    -- Second try: If it starts with a slash, modify and check again
    if string.sub(filename, 1, 1) == "/" then
        local filename = string.sub(filename, 2)
        if vim.fn.filereadable(filename) == 1 then
            vim.cmd("edit " .. filename)
            return
        end
    end

    print("File not found: " .. filename)
end

vim.g.mapleader = " "
vim.g.maplocalleader = " "

------------------------------------------------- COMMANDS ---------------------
map("t", "<esc>", "<C-\\><C-n>", { desc = "Switch to normal mode in terminal" })
map("n", "gf", "", { callback = _gf, desc = "Open file under cursor" })
map("n", "<leader>Q", "", { callback = close_buffer_or_exit, desc = "Close buffer and exit if none left" })
map("n", "<leader>f", vim.lsp.buf.format, { desc = "Format the whole file" })
-- map("n", "<leader><leader>", ":lua reload_config()<CR>", { desc = "Reload config" }) -- fails
map("n", "<leader>!", ":source %<CR>", { desc = "Source the current file" })
map("n", "<leader>pv", vim.cmd.Ex, { desc = "Open project view" })
map("n", "<Esc>", ":nohlsearch<CR><C-L><Esc>", { desc = "Clear search highlight" })
map("n", "<leader>g", ":!python %<CR>", { desc = "Run the current python script" })
map("n", "<leader>t", ":lua toggle_terminal()<CR>", { desc = "Toggle the terminal buffer or start one if not found" })
map("n", "<C-s>", ":update<CR>", { desc = "Save the file" })
map("n", "/", "q/i\\v", { desc = "Search history" })
map("v", "/", "q/i\\v", { desc = "Search history" })
map("n", ":", "q:i", { desc = "Command history" })
map("v", ":", "q:i", { desc = "Command history" })

map("n", "<C-d>", "<C-d>zz", { desc = "Scroll down and center" })
map("n", "<C-u>", "<C-u>zz", { desc = "Scroll up and center" })
map("n", "n", "nzzzv", { desc = "Keep search centered" })
map("n", "N", "Nzzzv", { desc = "Keep search centered" })
map("n", "zc", "zz", { desc = "Center the current line" })

map("n", "k", "v:count == 0 ? 'gk' : 'k'",
    { expr = true, desc = "Move the cursor up one visual line when count is not provided and by a real line when it is" })
map("n", "j", "v:count == 0 ? 'gj' : 'j'",
    { expr = true, desc = "Move the cursor down one visual line when count is not provided and by a real line when it is" })

-- diagnostics / errors
map("n", "<leader>o", "<cmd>copen<CR>", { desc = "Open quicklist" })
map("n", "<C-k>", "<cmd>cnext<CR>zz", { desc = "Jump to next error" })
map("n", "<C-j>", "<cmd>cprev<CR>zz", { desc = "Jump to previous error" })
map("n", "[d", "<cmd>lua vim.diagnostic.goto_prev()<CR>zz", { desc = "Jump to prev diagnostic message" })
map("n", "]d", "<cmd>lua vim.diagnostic.goto_next()<CR>zz", { desc = "Jump to next diagnostic message" })
map("n", "<leader><leader>", vim.diagnostic.open_float, { desc = "Open current diagnostic message" })
map("n", "<leader>q", vim.diagnostic.setloclist, { desc = "Open diagnostics list" })


------------------------------------------------- EDIT TEXT --------------------
map('n', '<A-d>', '"_d', { desc = "Delete without adding to the main register" })
map('n', '<A-c>', '"_c', { desc = "Change without adding to the main register" })
map('n', 'zz', _G.fix_spelling, { desc = "Fix spelling mistake" })
map('n', '<M-z><M-z>', _G.fix_spelling_and_jump, { desc = "Fix spelling mistake and jump to the next one" })

map({ "s", "v", "x" }, "P", '"_dP', { noremap = true, desc = "Don't replace yanked text when pasting over selection" })
map({ "s", "v", "x" }, "p", '"_dp', { noremap = true, desc = "Don't replace yanked text when pasting over selection" })

map("n", "[s", "[s", { desc = "Go to previous spelling mistake" })
map("n", "K", "i<CR><Esc>", { desc = "Split the line at the cursor position (inverse of J)" })

map("n", "<leader>s", [[:%s/\v/gc<Left><Left><Left>]],
    { noremap = true, silent = false, desc = "find and replace with regex" })
map("v", "<leader>s", [[:s/\v/gc<Left><Left><Left>]],
    { silent = false, desc = "find and replace with regex inside selection" })
map("n", "<leader>S", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]],
    { silent = false, desc = "Replace word under cursor" })

map("i", "@dts", "<C-R>=strftime('%Y-%m-%d %H:%M:%S')<CR>", { desc = "Insert datetime" })
map("i", "@dt", "<C-R>=strftime('%Y-%m-%d %H:%M')<CR>", { desc = "Insert datetime" })
map("i", "@d", "<C-R>=strftime('%Y-%m-%d')<CR>", { desc = "Insert date" })
map("i", "@ts", "<C-R>=strftime('%H:%M:%S')<CR>", { desc = "Insert time" })
map("i", "@t", "<C-R>=strftime('%H:%M')<CR>", { desc = "Insert time" })

map("n", "<F3>", [[:%s/\v(<\w)(\w+)/\=toupper(submatch(1)).tolower(submatch(2))/gc<CR>]],
    { desc = "Transform to title case" })
map("v", "<F3>", [[:s/\%V\v(<\w)(\w+)/\=toupper(submatch(1)).tolower(submatch(2))/gc<CR>]],
    { desc = "Transform to title case" })

-- Capitalise the first word in each sentence/row.
-- This does not make the other characters lowercase, so it does not affect abbreviations
-- e.g. wOrLd => WOrLd
map("n", "<F4>", [[:%s/\v((^<bar>[.!?*+#]\s+<bar>(\-\s+)<bar>\(\d+\)\s+)\W*[a-zA-z])/\=toupper(submatch(0))/g<CR>]],
    { desc = "Capitalise first word in each sentence/row" })
map("v", "<F4>", [[:s/\%V\v((^<bar>[.!?]\s+<bar>(\-\s+)<bar>\(\d+\)\s+)\W*[a-zA-z])/\=toupper(submatch(0))/g<CR>]],
    { desc = "Capitalise first word in each sentence/row" })

------------------------------------------------- EDIT LINES -------------------
map("v", "<A-j>", ":m '>+1<CR>gv=gv", { desc = "Move selected lines down" })
map("v", "<A-k>", ":m '<-2<CR>gv=gv", { desc = "Move selected lines up" })

map("n", "<leader>d",
    [[:%s/\v^(.*).*$\n//gc<Left><Left><Left><Left><Left><Left><Left><Left><Left><Left>]],
    { silent = false, desc = "delete the lines not containing the match" })
map("v", "<leader>d",
    [[:s/\v^(.*).*$\n//gc<Left><Left><Left><Left><Left><Left><Left><Left><Left><Left>]],
    { silent = false, desc = "delete the lines not containing the match" })

map("n", "<leader>D",
    [[:%s/\v^(.*)@\!.*$\n//gc<Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left>]],
    { silent = false, desc = "delete the lines not containing the match" })
map("v", "<leader>D",
    [[:s/\v^(.*)@\!.*$\n//gc<Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left>]],
    { silent = false, desc = "delete the lines not containing the match" })

map("v", "<s-tab>", "<", { desc = "Unindent" })
map("n", "<s-tab>", "<<", { desc = "Unindent" })
map("v", "<tab>", ">", { desc = "Indent" })
map("n", "<tab>", ">>", { desc = "Indent" })

map("n", "<F2>", [[:%s/\v^(\s*)(\w)/\=submatch(1).'- '.submatch(2) <CR> :nohlsearch<CR>]], { desc = "Prepend dash" })
map("v", "<F2>", [[:s/\v^(\s*)(\w)/\=submatch(1).'- '.submatch(2) <CR> :nohlsearch<CR>]], { desc = "Prepend dash" })

map("n", "<leader>r", ":g/^/m0<Esc>", { desc = "Reverse lines" })
map("v", "<leader>r", ":'<,'>!tac<Esc>", { desc = "Reverse lines" })
map("n", "<F9>", ":%sort i<CR>", { desc = "Sort (insensitive)" })
map("v", "<F9>", ":sort i<CR>", { desc = "Sort (insensitive)" })
map("n", "<F9><F9>", [[:silent! %s#\v\n$#\n#ge <bar> silent! %s#\v\n^(\s)#\n\1#ge <bar> %sort i <bar>%s#\%x00#\r#ge<CR>]],
    { desc = "Sort with groups" })


------------------------------------------------- MARKDOWN ---------------------
map("n", "<leader>l", "<cmd>lua markdown_link_history()<CR>", { desc = "Link history" })

-- Insert +/- in the checkboxes
-- map("v", "<leader>=", [[:s/\v^\[(\s*<bar>[+-])\]/[+]/<CR> :nohlsearch<CR>]])
-- map("n", "<leader>=", [[:.s/\v^\[(\s*<bar>[+-])\]/[+]/<CR> :nohlsearch<CR>]])
-- map("v", "<leader>-", [[:s/\v^\[(\s*<bar>[+-])\]/[-]/<CR> :nohlsearch<CR>]])
-- map("n", "<leader>-", [[:.s/\v^\[(\s*<bar>[+-])\]/[-]/<CR> :nohlsearch<CR>]])


-------------------------------------------------- JS --------------------------
map("i", "@cl", "console.log()<left>", { desc = "[.js] console.log()" })
map("i", "@ct", "console.table()<left>", { desc = "[.js] console.table()" })


------------------------------------------------- PYTHON -----------------------
map("i", "@ddd", "defaultdict(dict)<CR>", { desc = "[.py] defaultdict(dict)" })
map("i", "@ddl", "defaultdict(list)<CR>", { desc = "[.py] defaultdict(list)" })
map("i", "@dds", "defaultdict(set)<CR>", { desc = "[.py] defaultdict(set)" })
map("i", "@ipp", "from pprint import pprint<CR>", { desc = "[.py] from pprint import pprint" })
map("i", "@idd", "from collections import defaultdict<CR>", { desc = "[.py] from collections import defaultdict" })
map("i", "@main",
    "from pprint import pprint\ndef main(): print(\"hello\")\nif __name__ == \"__main__\": main()<CR><C-o>:w<CR>",
    { desc = "[.py] if __name__ == \"__main__\"" })


------------------------------------------------- DISABLED ---------------------
map("n", "Q", "<nop>", { desc = "ThePrimeagen said so" })
map("n", "s", "<nop>", { desc = "I don't use substitute char and it gets in the way" })
map("n", "S", "<nop>", { desc = "I don't use substitute line and it gets in the way" })

for _, mode in ipairs({ "n", "v" }) do
    for _, key in ipairs({ "<Up>", "<Down>", "<Left>", "<Right>" }) do
        map(mode, key, "<nop>", { desc = "Disable arrow keys" })
    end
end

------------------------------------------------- TEMPORARY --------------------

-- map("n", "<C-f>", { desc = "<cmd>silent !tmux neww tmux-sessionizer<CR>" })


-- map("n", "<leader><leader>", [[:%s/\v^(\s*)/\=submatch(1).(line('.') - 1 + 1).'. ' <CR> :nohlsearch<CR>]],
--     { desc = "Prepend numbers" })
-- map("v", "<leader><leader>", [[:s/\v^(\s*)/\=submatch(1).(line('.') - 1 + 1).'. ' <CR> :nohlsearch<CR>]],
--     { desc = "Prepend number" })
-- map("n", "<leader><leader>", [[:execute ':%s/\v^\s*/\=printf("%d. ", line('.') - line("'<") + 1)/ <CR> :nohlsearch<CR>]],
--     { desc = "Prepend numbered list" })
