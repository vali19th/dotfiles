return {
    {
        "nvim-treesitter/nvim-treesitter",
        config = function()
            require 'nvim-treesitter.configs'.setup {
                -- A list of parser names, or "all" (the five listed parsers should always be installed)
                ensure_installed = {
                    "lua", "vim", "vimdoc", "query",
                    "python", "requirements",
                    "gitignore", "c", "bash", "regex", "sql",
                    "csv", "json", "toml", "xml", "latex",
                    "javascript", "css", "html",
                },
                sync_install = false, -- synchronous or asynchronous install
                auto_install = true,  -- install missing parsers
                debug = true,
                fold = { enable = true },
                highlight = {
                    enable = true,
                    disable = { "markdown" },                 -- some of my .md files are 5-10k lines long, which makes treesitter too slow
                    additional_vim_regex_highlighting = true, -- fix indentation and "ci[" going crazy if I don't close square brackets in Python strings
                },
                incremental_selection = {
                    enable = true,
                    keymaps = {
                        init_selection = "gnn",
                        node_incremental = "grn",
                        scope_incremental = "grc",
                        node_decremental = "grm",
                    },
                },
            }
        end,
    },
    "nvim-treesitter/nvim-treesitter-context",
    {
        "nvim-treesitter/nvim-treesitter-textobjects",
        dependencies = { "nvim-treesitter/nvim-treesitter" },
        event = "BufRead",
        config = function()
            require 'nvim-treesitter.configs'.setup {
                textobjects = {
                    select = {
                        enable = true,
                        lookahead = true, -- Automatically jump forward to the object
                        include_surrounding_whitespace = false,
                        keymaps = {
                            ["af"] = { query = "@function.outer", desc = "Select around function" },
                            ["if"] = { query = "@function.inner", desc = "Select inside function" },
                            ["aC"] = { query = "@class.outer", desc = "Select around class" },
                            ["iC"] = { query = "@class.inner", desc = "Select inside class" },
                            ["ab"] = { query = "@block.outer", desc = "Select around block" },
                            ["ib"] = { query = "@block.inner", desc = "Select inside block" },
                            ["ac"] = { query = "@comment.outer", desc = "Select around comment" },
                            ["ic"] = { query = "@comment.inner", desc = "Select inside comment" },
                            ["ai"] = { query = "@conditional.outer", desc = "Select around conditional" },
                            ["ii"] = { query = "@conditional.inner", desc = "Select inside conditional" },
                        },
                        selection_modes = {
                            ['@parameter.outer'] = 'v', -- charwise
                            ['@function.inner'] = 'V',  -- linewise
                            ['@function.outer'] = 'V',  -- linewise
                            ['@class.outer'] = 'V',     -- linewise
                        },
                    },
                },
            }
        end
    },
}
