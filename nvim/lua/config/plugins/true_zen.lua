return {
    "Pocco81/true-zen.nvim",
    config = function()
        local truezen = require('true-zen')
        truezen.setup({
            modes = {
                ataraxis = {
                    minimum_writing_area = {
                        width = 60,
                        height = 44,
                    },
                    padding = {
                        left = 999,
                        right = 999,
                        top = 0,
                        bottom = 0,
                    }
                },
                minimalist = {
                    options = {
                        number = true,
                        relativenumber = true,
                        signcolumn = "yes",
                        showmode = true,
                        ruler = false,
                        numberwidth = 1
                    }
                }
            },
        })
        vim.keymap.set('n', '<leader>z', truezen.ataraxis, { noremap = true })
    end
}

