return {
    "VonHeikemen/lsp-zero.nvim",
    branch = "v3.x",
    dependencies = {
        "williamboman/mason.nvim",
        "williamboman/mason-lspconfig.nvim",
        "neovim/nvim-lspconfig",
        "hrsh7th/cmp-nvim-lsp",
        "hrsh7th/cmp-buffer",
        "hrsh7th/cmp-path",
        -- "hrsh7th/cmp-cmdline",
        "hrsh7th/nvim-cmp",
        "L3MON4D3/LuaSnip",
        "saadparwaiz1/cmp_luasnip",
    },
    config = function()
        local lsp = require("lsp-zero")
        lsp.preset("recommended")
        require("mason").setup({})
        require("mason-lspconfig").setup({
            ensure_installed = {
                "bashls",
                "biome",
                "cssls",
                "gopls",
                "html",
                "htmx",
                "jinja_lsp",
                "jsonls",
                "lua_ls",
                "pylsp",
            },
            handlers = { lsp.default_setup },
        })

        local cmp = require("cmp")
        local cmp_select = { behavior = cmp.SelectBehavior.Select }

        cmp.setup({
            snippet = {
                expand = function(args)
                    require("luasnip").lsp_expand(args.body)
                end,
            },
            sources = cmp.config.sources({
                { name = "nvim_lsp" },
                { name = "buffer" },
                { name = "cody" },
                { name = "path" },
                -- { name = "cmdline" },
                { name = "luasnip" },
            }),
            mapping = cmp.mapping.preset.insert({
                ["<S-Tab>"] = cmp.mapping.select_prev_item(cmp_select),
                ["<Tab>"] = cmp.mapping.select_next_item(cmp_select),
                ["<CR>"] = cmp.mapping.confirm({ select = false }),
                ["<esc>"] = cmp.mapping.abort(),
            })
        })

        -- Fix Undefined global "vim"
        lsp.configure("lua_ls", {
            settings = {
                Lua = {
                    diagnostics = {
                        globals = { "vim" }
                    }
                }
            }
        })

        lsp.set_preferences({
            suggest_lsp_servers = false,
            sign_icons = {
                error = "E",
                warn = "W",
                hint = "H",
                info = "I"
            }
        })

        lsp.on_attach(function(_, bufnr)
            local opts = { buffer = bufnr, remap = false }
            vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
            vim.keymap.set("n", "gr", function() vim.lsp.buf.references() end, opts)
            vim.keymap.set("n", "<leader>h", function() vim.lsp.buf.hover() end, opts)
            vim.keymap.set("n", "<leader>e", function() vim.diagnostic.open_float() end, opts)
            vim.keymap.set("n", "<leader>r", function() vim.lsp.buf.references() end, opts)
            vim.keymap.set("n", "<leader>c", function() vim.lsp.buf.rename() end, opts)
        end)

        lsp.setup()
        vim.diagnostic.config({ virtual_text = true })

        require("lspconfig").bashls.setup({})
        require("lspconfig").biome.setup({})
        require("lspconfig").cssls.setup({})
        require("lspconfig").gopls.setup({})
        require("lspconfig").html.setup({ filetypes = { "html" } })
        require("lspconfig").htmx.setup({ filetypes = { "html" } })
        require("lspconfig").jinja_lsp.setup({ filetypes = { "html" } })
        require("lspconfig").jsonls.setup({})
        require("lspconfig").lua_ls.setup(lsp.nvim_lua_ls())

        require('lspconfig').pylsp.setup({
            settings = {
                pylsp = {
                    plugins = {
                        pycodestyle = {
                            ignore = { "W391", "W503", "E203", "E241", "E271", "E272", "E501", "E701", "E704" },
                            maxLineLength = 120,
                        },
                    },
                }
            }
        })
    end
}
