return {
    -- "laytan/cloak.nvim", -- for hiding sensitive info
    "nvim-lua/plenary.nvim",
    -- "github/copilot.vim",
    "ThePrimeagen/vim-be-good",
    -- {
    --     "sourcegraph/sg.nvim",
    --     dependencies = { "nvim-lua/plenary.nvim" },
    --     config = function()
    --         require("sg").setup({ enable_cody = true, event = "InsertEnter" })

    --         -- TODO: add shortcuts
    --         -- :CodyAsk: Ask a question about the current selection
    --         -- :CodyChat {title}: Starts a new Cody chat, with an optional {title}
    --         -- :CodyRestart: Restarts Cody and Sourcegraph
    --         -- :CodyTask {task_description}: Instructs Cody to perform a task on a selected text
    --         -- :CodyTaskAccept: Accepts the current CodyTask
    --         -- :CodyTaskNext: Cycles to the next CodyTask
    --         -- :CodyTaskPrev: Cycles to the previous CodyTask
    --         -- :CodyTaskView: Opens the last active CodyTask
    --         -- :CodyToggle: Toggle to the current Cody Chat window
    --     end,
    -- },
    {
        "tpope/vim-fugitive",
        config = function()
            vim.keymap.set("n", "<leader>gs", vim.cmd.Git)
        end
    },
    {
        "lewis6991/gitsigns.nvim",
        config = function()
            require("gitsigns").setup()
        end
    },
    {
        "numToStr/Comment.nvim",
        config = function()
            require("Comment").setup({ ignore = "^$" })
        end
    },
    {
        "lukas-reineke/indent-blankline.nvim",
        main = "ibl",
        opts = function(_, opts)
            return require("indent-rainbowline").make_opts(opts)
        end,
        dependencies = {
            "TheGLander/indent-rainbowline.nvim",
        },
    },
    {
        "ThePrimeagen/refactoring.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-treesitter/nvim-treesitter"
        }
    },
    {
        "folke/trouble.nvim",
        config = function()
            require("trouble").setup({ icons = false })
            vim.keymap.set("n", "<leader>tt", function()
                require("trouble").toggle()
            end)

            vim.keymap.set("n", "[", function()
                require("trouble").next({ skip_groups = true, jump = true })
            end)

            vim.keymap.set("n", "[", function()
                require("trouble").previous({ skip_groups = true, jump = true })
            end)
        end
    },
}
