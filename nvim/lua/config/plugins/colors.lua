local function apply_custom_colors()
    vim.api.nvim_set_hl(0, "Folded", { foreground = "#00ff00" })
    vim.api.nvim_set_hl(0, "Identifier", { link = "" })

    if vim.o.background == "dark" then
        vim.api.nvim_set_hl(0, "Normal", { foreground = "#ebdbb2", background = "#1d2021" })
        vim.api.nvim_set_hl(0, "Visual", { foreground = "#dddddd", background = "#aa0000" })
        vim.api.nvim_set_hl(0, "LineNr", { foreground = "#00bb00", bold = true })
        vim.api.nvim_set_hl(0, "CursorLineNr", { foreground = "#FBC852", background = "#3c3836", bold = true })
        vim.api.nvim_set_hl(0, "Folded", { foreground = "#00cc00", bold = true })
        vim.api.nvim_set_hl(0, "Todo", { foreground = "#000000", background = "#a30202" })
    else
        vim.api.nvim_set_hl(0, "Normal", { foreground = "#111111", background = "#fbf1c7" })
        vim.api.nvim_set_hl(0, "LineNr", { foreground = "#550000", bold = true })
        vim.api.nvim_set_hl(0, "CursorLineNr", { foreground = "#FBC852", background = "#3c3836", bold = true })
        vim.api.nvim_set_hl(0, "Folded", { foreground = "#a30202", bold = true })
        vim.api.nvim_set_hl(0, "Todo", { foreground = "#ffffff", background = "#a30202" })
    end

    vim.api.nvim_set_hl(0, "CursorColumn", vim.api.nvim_get_hl_by_name("CursorLineNr", false))
end

local function set_background_based_on_time()
    local day_start = tonumber(os.getenv("_DAY_START"))
    local day_end = tonumber(os.getenv("_DAY_END"))
    local current_hour = tonumber(os.date("%H"))

    if day_start <= current_hour and current_hour < day_end then
        vim.opt.background = "light"
    else
        vim.opt.background = "dark"
    end

    apply_custom_colors()
end

function _G.alternate_theme()
    if vim.o.background == "dark" then
        vim.o.background = "light"
    else
        vim.o.background = "dark"
    end
    apply_custom_colors()
end

return {
    "ellisonleao/gruvbox.nvim",
    config = function()
        require("gruvbox").setup({
            contrast = "hard",
        })
        vim.cmd("colorscheme gruvbox")
        set_background_based_on_time()
        vim.keymap.set('n', '<leader>T', ':lua alternate_theme()<CR>', { noremap = true, silent = true })
    end
}
