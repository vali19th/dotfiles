local function _extract_selection()
    local old_reg = vim.fn.getreg("a")          -- Save the current contents of register a
    local old_reg_type = vim.fn.getregtype("a") -- Save the current type of register a
    vim.cmd('silent! normal! "ay')              -- Yank the visually selected text into register a

    local selection = vim.fn.getreg("a")        -- Get the yanked text from register a
    vim.fn.setreg("a", old_reg, old_reg_type)   -- Restore the original contents and type of register a
    return selection
end

return {
    "nvim-telescope/telescope.nvim",
    tag = "0.1.4",
    dependencies = "nvim-lua/plenary.nvim",
    config = function()
        require("telescope").setup({
            defaults = {
                file_ignore_patterns = { "__pycache__", ".venv", ".git" }
            }
        })

        local builtin = require("telescope.builtin")
        vim.keymap.set("n", "<leader>fv", ":Ex<CR>", { noremap = true, silent = true })
        vim.keymap.set("n", "<leader>b", builtin.buffers, {})
        vim.keymap.set("n", "<leader>h", builtin.help_tags, {})

        -- search file names
        local cmd = {
            "rg", "--files", "--sort=path",
            "--glob", "*",
            "--glob", "!.*",
            "--glob", "!*.pyc",
            "--glob", ".env*",
            "--glob", ".gitlab-ci.yml",
            "--glob", ".pre-commit-config.yaml"
            -- don't bother adding .gitignore because it won't be shown no matter how much you try
        }
        vim.keymap.set("n", "<leader>ff", function()
            require("telescope.builtin").git_files({ find_command = cmd })
        end)

        vim.keymap.set("n", "<leader>fF", function()
            require("telescope.builtin").find_files({ find_command = cmd })
        end)

        -- search file contents
        vim.keymap.set("n", "<leader>fs", builtin.live_grep, {})
        vim.keymap.set("v", "<leader>w", function()
            builtin.grep_string({ search = _extract_selection() });
        end)
        vim.keymap.set("n", "<leader>w", function()
            builtin.grep_string({ search = vim.fn.expand("<cword>") });
        end)
        vim.keymap.set("n", "<leader>W", function()
            builtin.grep_string({ search = vim.fn.expand("<cWORD>") });
        end)
    end
}
