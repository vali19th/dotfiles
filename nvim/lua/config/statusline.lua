local function diagnostics_status()
    local diagnostics = vim.diagnostic.get(0)
    local counts = { E = 0, W = 0, I = 0, H = 0 }
    local x = {}

    for _, diag in pairs(diagnostics) do
        table.insert(x, vim.inspect(diag))
        if diag.severity == vim.diagnostic.severity.ERROR then
            counts.E = counts.E + 1
        elseif diag.severity == vim.diagnostic.severity.WARN then
            counts.W = counts.W + 1
        elseif diag.severity == vim.diagnostic.severity.INFO then
            counts.I = counts.I + 1
        elseif diag.severity == vim.diagnostic.severity.HINT then
            counts.H = counts.H + 1
        end
    end

    return string.format("%dE %dW %dI %dH", counts.E, counts.W, counts.I, counts.H)
end

local function vim_mode()
    local modes = {
        ['n']  = 'NORMAL',
        ['no'] = 'N·OPERATOR PENDING',
        ['v']  = 'VISUAL',
        ['V']  = 'V·LINE',
        ['']  = 'V·BLOCK',
        ['s']  = 'SELECT',
        ['S']  = 'S·LINE',
        ['']  = 'S·BLOCK',
        ['i']  = 'INSERT',
        ['ic'] = 'INSERT',
        ['R']  = 'REPLACE',
        ['Rv'] = 'V·REPLACE',
        ['c']  = 'COMMAND',
        ['cv'] = 'VIM EX',
        ['ce'] = 'EX',
        ['r']  = 'PROMPT',
        ['rm'] = 'MORE',
        ['r?'] = 'CONFIRM',
        ['!']  = 'SHELL',
        ['t']  = 'TERMINAL'
    }
    return modes[vim.fn.mode()] or vim.fn.mode()
end

local function git_branch()
    local handle = io.popen("git rev-parse --abbrev-ref HEAD 2>/dev/null")
    if handle == nil then return 'N/A' end

    local result = handle:read("*a")
    handle:close()
    return result ~= '' and result:gsub("\n", "") or 'N/A'
end

local function git_changes()
    local file = vim.fn.expand('%:p') -- Gets the full path of the current file
    if file == '' or file == nil then return 'N/A' end

    local command = "git diff --numstat " .. file .. " 2>/dev/null"
    local handle = io.popen(command)
    if handle == nil then return 'N/A' end

    local result = handle:read("*a")
    handle:close()

    local added, removed = result:match("(%d+)%s+(%d+)")
    added = added or 0
    removed = removed or 0
    return "+" .. added .. " -" .. removed
end

local function set_statusline()
    local statusline = {
        vim_mode(),
        diagnostics_status(),
        git_branch() .. " " .. git_changes(),
        "%l:%c %t", -- line:col filename
    }
    vim.o.statusline = table.concat(statusline, " | ")
end

vim.api.nvim_create_autocmd(
    { "BufEnter", "BufReadPost", "TextChanged" },
    { pattern = "*", callback = set_statusline }
)

