local function check_log(path, pattern, timeout)
    timeout = timeout or 10
    local start_time = os.time()

    repeat
        local str = vim.fn.execute("!tail -n 1 " .. path)
        if str and string.match(str, pattern) then
            return true
        end
        vim.fn.timer_start(100, nil)
    until os.time() - start_time >= timeout

    return false
end

vim.keymap.set("n", "<leader>jr", function()
    vim.cmd("silent !python -m jupyter_ascending.requests.restart --filename " .. vim.fn.expand("%:p"))
    local path = "/tmp/jupyter_ascending/log.log"
    local pattern = "jupyter_ascending.handlers.jupyter_notebook:start_notebook_server_in_thread:.* ==> Success"

    if check_log(path, pattern) then
        vim.cmd("silent !python -m jupyter_ascending.requests.execute_all --filename " .. vim.fn.expand("%:p"))
    else
        print("Failed to restart")
    end
end)

vim.keymap.set("n", "<leader>jo", function()
    local file = string.gsub(vim.fn.expand("%:p"), "py", "ipynb")
    vim.cmd("silent !python -m jupyter notebook " .. file .. " &")
end)

vim.keymap.set("n", "<leader><CR>", function()
    vim.cmd("silent !python -m jupyter_ascending.requests.execute --filename %:p --line " .. vim.fn.line("."))
end)

